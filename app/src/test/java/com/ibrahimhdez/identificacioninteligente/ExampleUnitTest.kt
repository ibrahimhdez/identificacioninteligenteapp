package com.ibrahimhdez.identificacioninteligente

import android.os.Build.VERSION_CODES.LOLLIPOP
import com.ibrahimhdez.identificacioninteligente.faceAPI.*
import com.ibrahimhdez.identificacioninteligente.database.DBHelper
import com.ibrahimhdez.identificacioninteligente.utils.Data
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(LOLLIPOP))
class ExampleUnitTest {
    lateinit var controlador: Controlador
    lateinit var group: GroupPerson
    lateinit var person: Person

    @Before
    fun iniciarTests() {
        this.controlador = Controlador()
        Comunicador.controlador = this.controlador
        this.group = GroupPerson("futbolistas", "grupo_prueba")
        this.person = Person("Messi", "", "")
    }

    @Test
    fun databaseTest() {
        val db = DBHelper(RuntimeEnvironment.application)

        println(db.getTableAsString("PERSONS"))
    }

    @Test
    fun enumTests() {
        val data = Data(this.group, this.person, "323e114f-5bec-486d-8efb-08cac1a13379", "src/main/java/com/ibrahimhdez/identificacioninteligente/Images/pedro.jpg")
        this.controlador.data = data

        this.controlador.train()
        this.controlador.identify()
        println(controlador.json)
    }
}
