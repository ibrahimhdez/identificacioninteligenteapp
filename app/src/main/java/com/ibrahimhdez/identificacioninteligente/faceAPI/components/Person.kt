package com.ibrahimhdez.identificacioninteligente.faceAPI.components

/**
 * Clase que gestiona las Persons
 * @author Ibrahim Hernández Jorge
 */
class Person {
    var name: String
    var personId: String? = null
    var voiceId = ""
    var persistedFaceId: String? = null
    var description: String? = null

    constructor(name: String){
        this.name = name
    }

    constructor(id: String, name: String) {
        this.name = name
        this.personId = id
    }

    constructor(name: String, personId: String, persistedFaceId: String) {
        this.name = name
        this.personId = personId
        this.persistedFaceId = persistedFaceId
    }

    override fun toString(): String {
        return "Nombre: $name \nPersonID: $personId \nPersistedFaceID: $persistedFaceId"
    }
}
