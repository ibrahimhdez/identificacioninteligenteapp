package com.ibrahimhdez.identificacioninteligente.faceAPI

import android.app.AlertDialog
import android.content.Context
import android.database.Cursor
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import org.json.JSONArray
import org.json.JSONObject

/**
 * Clase que sincroniza la aplicación con la Nube
 * @author Ibrahim Hernández Jorge
 */
class CloudSynchronize: AsyncResponse {
    private var groups: ArrayList<GroupPerson> = ArrayList()
    private var localGroups: ArrayList<GroupPerson> = ArrayList()
    private var oldLocalGroups: ArrayList<GroupPerson> = ArrayList()
    private var progress: AlertDialog? = null
    private var context: Context? = null
    private var oldGroups: Boolean = false
    private var iterator: Int? = null

    fun obtainGroups(jsonArray: JSONArray, progress: AlertDialog, context: Context) {
        this.context = context
        this.iterator = 1
        this.progress = progress
        var cursor: Cursor?
        var json: JSONObject
        var group: GroupPerson?
        this.groups.clear()
        this.localGroups = obtainLocalGroups() //Contiene los grupos que existen actualmente en local

        for(i in 0 until jsonArray.length()) {
            //Objeto JSON que contendrá uno de los grupos de la nube según el iterador del bucle
            json = jsonArray[i] as JSONObject
            //Consultamos en la base de datos local si este grupo de la nube existe
            cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_GROUPS, arrayOf(DBContract.DBEntry.ID), arrayOf(json.getString("personGroupId")))
            //Si el count del cursor está a 0 quiere decir que este grupo no está en local
            //Por lo que tendrá que ser insertado
            if(cursor.count == 0) {
                //Creamos un objeto grupo con el grupo que no existe en local para insertarlo
                group = GroupPerson(json.getString("personGroupId"), json.getString("name"))
                //Primero lo añadimos al grupo de grupos nuevos, para luego insertar las personas que lo contengan
                this.groups.add(group)
                //Luego insertamos en la base de datos local este nuevo grupo
                DBComunicator.getInstance().insertGroupPerson(group)
            }
            //Si el count del cursor no está a 0, quiere decir que está en local, por lo que se eliminará del grupo para que no se borre
            //y se añadirá a otro que contendrá los que ya existen, así se puede comprobar si están actualizados con la nube
            else {
                //Comprobará cual es de todos los grupos locales
                for(j in 0 until this.localGroups.size)
                    //Si coinciden los ids
                    if(this.localGroups[j].id == json.getString("personGroupId")) {
                        //Lo añadimos a los grupos locales nuevos
                        this.oldLocalGroups.add(this.localGroups[j])
                        //Lo borramos de grupos obsoletos
                        this.localGroups.remove(this.localGroups[j])
                        break //Rompemos ejecución para que el bucle pare y no de error por temas de índices de iteración
                    }
            }
        }
        //Si quedaron grupos en el ArrayList localGroups, hay que eliminarlos, ya que están en local pero no en nube (fueron borrados anteriormente)
        if(this.localGroups.size > 0)
            deleteGroups()

        //Si hay grupos en la nube que ya estaban, hay que refrescarlos por si tienen cambios (personas nuevas, caras nuevas, ...)
        if(this.oldLocalGroups.size > 0)
            refreshOldLocalGroups()

        //Si hay grupos en este ArrayList quiere decir que hay grupos que están en nube y no en local, por lo que tienen que ser añadidos.
        if(this.groups.size > 0)
            obtainArrayPersons()

        //Si no hay, se da por finalizada la sincronización y se cierra el ProgressDialog
        else
            this.progress!!.dismiss()

    }

    private fun refreshOldLocalGroups() {
        this.oldGroups = true

        //Mientras hayan grupos que ya estaban en local después de sincronizar, vamos comprobando uno por uno por si hay que actualizar personas
        if(this.oldLocalGroups.size > 0) {
            Comunicador.data!!.groupPerson = this.oldLocalGroups[0]
            Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_GET_LIST_PERSONS, Comunicador.data!!, this)
        }
        //Si ya no hay más oldGroups, ponemos a false el Booleano que el delegado pase a otra acción
        else {
            this.progress!!.dismiss()
            this.oldGroups = false
        }
    }

    private fun obtainLocalGroups(): ArrayList<GroupPerson> {
        val arrayGroups = ArrayList<GroupPerson>()
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_GROUPS} ")

        while(cursor.moveToNext())
            arrayGroups.add(GroupPerson(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME))))

        return arrayGroups
    }

    /**
     * Método que se ejecutará cuando hayan grupos en nube que no estén en local
     */
    private fun obtainArrayPersons() {
        //Si aun quedan grupos, descargamos la lista de personas para insertarlas
        if(this.groups.size > 0) {
            Comunicador.data!!.groupPerson = this.groups[0]
            Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_GET_LIST_PERSONS, Comunicador.data!!, this)
        }
        else {
            //Si hay grupos en el ArrayList oldLocalGroups se comprueba si sus personas están actualizadas
            if(this.oldLocalGroups.size > 0)
                refreshOldLocalGroups()
            else
                this.progress!!.dismiss()
        }
    }

    override fun processFinish(response: Int) {
        //Si estamos trabajando con los grupos que ya estaban en local para actualizarlos
        if(this.oldGroups) {
            //Refrescar las personas de uno de los grupos
            refreshInsertPerson(Comunicador.controlador!!.arrayJsonString!!)
            //Ya actualizamos un oldGroup, pasamos al siguiente si lo hay
            refreshOldLocalGroups()
        }
        //Se ejecutará el else cuando estemos tratando con grupos que están en nube y no en local
        else {
            //Insertamos las personas en el grupo actual
            //TODO ver si el método del if hace lo mismo que este
            insertPersons(Comunicador.controlador!!.arrayJsonString!!)
            //Volvemos a llamar a la función que invocó este método para pasar al siguiente grupo
            obtainArrayPersons()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(context, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método que se ejecuta cuando se descargan las personas de un grupo que se ha creado nuevo debido a una sincronización con la nube.
     * @param jsonArray Contiene las personas a insertar en el grupo actual
     */
    private fun insertPersons(jsonArray: JSONArray) {
        var json: JSONObject
        //Recorremos el array de objetos JSON. Cada objeto es una persona que será insertada en local en el grupo actual
        for(i in 0 until jsonArray.length()) {
            json = jsonArray[i] as JSONObject
            var persistedFaceId = json.getJSONArray("persistedFaceIds").toString().replace("[", "")
            persistedFaceId = persistedFaceId.replace("]", "")
            val person = Person(json.getString("name"), json.getString("personId"), persistedFaceId)
            DBComunicator.getInstance().insertPerson(person, this.groups[0], "", Comunicador.defaultPassword)
            if(person.persistedFaceId != "") {
                DBComunicator.getInstance().insertFace("Face ${this.iterator!!}", person)
                this.iterator!!.inc()
            }
        }
        //Una vez hayamos acabado con el grupo actual lo eliminamos para pasar al siguiente si lo hay
        this.groups.removeAt(0)
    }

    /**
     * Método que actualiza las personas de un solo grupo con la nube
     * @param jsonArray Array JSON donde cada objeto es una persona perteneciente al grupo actual
     */
    private fun refreshInsertPerson(jsonArray: JSONArray) {
        var json: JSONObject
        var cursor: Cursor?
        var person: Person
        var persistedFaceId: String
        //Obtenemos los grupos locales para descartar los que están en local pero no en la nube
        val oldLocalPerson = obtainLocalPersons(this.oldLocalGroups[0])

        //Recorremos el array de objetos JSON para obtener todas las personas
        for(i in 0 until jsonArray.length()) {
            json = jsonArray[i] as JSONObject
            persistedFaceId = json.getJSONArray("persistedFaceIds").toString().replace("[", "")
            persistedFaceId = persistedFaceId.replace("]", "")
            //Persona actual de la nube
            person = Person(json.getString("name"), json.getString("personId"), persistedFaceId)

            //Comprobamos si esta persona está ya en local
            cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_PERSONS, arrayOf(DBContract.DBEntry.ID), arrayOf(person.personId!!))
            //Si el count del cursor es 0 quiere decir que la persona no está en local, por lo que, la persona y las caras se insertan
            if(cursor.count == 0) {
                DBComunicator.getInstance().insertPerson(person, this.oldLocalGroups[0], "", Comunicador.defaultPassword)
                if(person.persistedFaceId != "") {
                    DBComunicator.getInstance().insertFace("Face ${this.iterator!!}", person)
                    this.iterator!!.inc()
                }
            }
            //Si no, es que la persona está, y hay que actualizarla (por si se le han añadido caras)
            else {
                //Borramos he insertamos de nuevo la persona de la nube con todos los datos nuevos
                //TODO hacer que si está actualizada, no se borre e inserte de nuevo por temas de optimización
                DBComunicator.getInstance().deletePerson(person)
                DBComunicator.getInstance().insertPerson(person, this.oldLocalGroups[0], "", Comunicador.defaultPassword)
                if(person.persistedFaceId != "") {
                    DBComunicator.getInstance().insertFace("Face ${this.iterator!!}", person)
                    this.iterator!!.inc()
                }

                //Sacamos esta persona de la lista de personas que ya estaban en local
                //Las personas que queden en esta lista serán eliminadas, ya que no están en la nube
                for(j in 0 until oldLocalPerson.size) {
                    if(oldLocalPerson[j].personId == person.personId) {
                        oldLocalPerson.remove(oldLocalPerson[j])
                        break
                    }
                }
            }
        }
        //Si después de recorrer todas las personas de la nube, queda alguna en local que no ha sido marcada, la eliminamos
        if(oldLocalPerson.size > 0)
            deleteOldPersons(oldLocalPerson)
        //Eliminamos el grupo que acabamos de tratar de la lista de grupos que ya se encontraban en local. Así pasamos a comprobar el siguiente en el caso de que existan más
        this.oldLocalGroups.removeAt(0)
    }

    private fun obtainLocalPersons(group: GroupPerson): ArrayList<Person> {
        val arrayPersons = ArrayList<Person>()
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_PERSONS} WHERE ${DBContract.DBEntry.GROUP_ID} = '${group.id}'")
        var person: Person

        while(cursor.moveToNext()) {
            person = if(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.GROUP_ID)) != null)
                Person(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.GROUP_ID)))
            else
                Person(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)))
            arrayPersons.add(person)
        }
        return arrayPersons
    }

    private fun deleteGroups() {
        for(i in 0 until this.localGroups.size)
            DBComunicator.getInstance().deleteGroupPerson(this.localGroups[i])
    }

    private fun deleteOldPersons(persons: ArrayList<Person>) {
        for(i in 0 until persons.size)
            DBComunicator.getInstance().deletePerson(persons[i])
    }
}