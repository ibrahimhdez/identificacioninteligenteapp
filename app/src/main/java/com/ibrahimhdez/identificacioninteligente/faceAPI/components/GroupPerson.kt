package com.ibrahimhdez.identificacioninteligente.faceAPI.components

/**
 * Clase que gestiona los GroupPerson
 * @author Ibrahim Hernández Jorge
 */
class GroupPerson(var id: String, var name: String){
    override fun toString(): String {
        return "GroupPersonID: ${this.id}\n Name: ${this.name}"
    }
}