package com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks

import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import org.json.JSONObject

/**
 * Interfaz que controla las respuestas de las tareas asíncronas.
 * La implementarán las clases que necesiten hacer peticiones web.
 * @author Ibrahim Hernández Jorge
 */
interface AsyncResponse {
    fun processFinish(response: Int)

    fun processFinish(consulta: Consultas.ApiConsults) {}

    fun processFinish(json: JSONObject) {}

    fun processFailed()
}