package com.ibrahimhdez.identificacioninteligente.faceAPI

import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.OutputStreamAsyncTask
import com.ibrahimhdez.identificacioninteligente.utils.Data

import java.net.HttpURLConnection
import java.net.URL

/**
 * Clase que gestiona las Consultas a la API
 * @author Ibrahim Hernández Jorge
 */
class Consultas {
    /**
     * Enum que estructura el JSON para realizar la petición HTTP a la API
     * @param header Header del JSON que contendrá el tipo del body y la clave de la API
     * @param body Body del JSON
     */
    enum class JsonConsults constructor(var header: String, var body: String){
        JSON_CREATE_PERSONGROUP("Content-Type,application/json", "{\"name\":\"GET_GROUPNAME\",\"userData\":\"User-provided data attached to the PersonGroup.\" }"),
        JSON_CREATE_PERSON("Content-Type,application/json", "{\"name\":\"GET_PERSONNAME\",\"userData\":\"User-provided data attached to the PersonGroup.\" }"),
        JSON_ADD_FACE_2_PERSON("Content-Type,application/octet-stream", ""),
        JSON_TRAIN("Content-Type,application/json", ""),
        JSON_TRAINING("Content-Type,application/json", ""),
        JSON_DETECT_FACE("Content-Type,application/octet-stream", ""),
        JSON_IDENTIFY("Content-Type,application/json", "{\"personGroupId\":\"GET_GROUPID\",\"faceIds\":[\"GET_FACEID\"],\"maxNumOfCandidatesReturned\": 1,\"confidenceThreshold\": 0.5}"),
        JSON_EMPTY("", "");

        val headerKey = "Ocp-Apim-Subscription-Key,ea9193794d904ddeb5cb10d082327ac3" //Clave de la API
        private val bodyCopy = body

        /**
         * Método que reemplaza en el body del JSON los valores necesarios para hacer la consulta personalizada
         */
        fun replaceData(data: Data) {
            if(this.body != this.bodyCopy)
                this.body = this.bodyCopy

            if (this.body.contains("GET_GROUPNAME"))
                this.body = this.body.replace("GET_GROUPNAME", data.groupPerson!!.name)

            if (this.body.contains("GET_PERSONNAME"))
                this.body = this.body.replace("GET_PERSONNAME", data.person!!.name)

            if (this.body.contains("GET_GROUPID"))
                this.body = this.body.replace("GET_GROUPID", data.groupPerson!!.id)

            if (this.body.contains("GET_PERSONID"))
                this.body = this.body.replace("GET_PERSONID", data.person!!.personId!!)

            if (this.body.contains("GET_FACEID"))
                this.body = this.body.replace("GET_FACEID", data.faceIds!!)
        }
    }

    /**
     * Enum que estructura las peticiones HTTP
     * @param requestMethod Método de la petición (Puede tener los valores: PUT, POST, GET y DELETE)
     * @param modulo Módulo de la petición web
     * @param groupId ID asociado al GroupPerson con el que queremos trabajar
     * @param accion acción de la petición
     * @param personId ID asociado a la Person con la que queremos trabajar
     * @param parametros Últimos parámetros de la petición web
     * @param json JSON que acompaña a la petición HTTP
     */
    enum class ApiConsults constructor(var requestMethod: String, private var modulo: String, private var groupId: String, private var accion: String, private var personId: String, private var parametros: String, var respuestaJSONArray: Boolean, var json: JsonConsults){
        CONSULT_CREATE_PERSONGROUP("PUT","persongroups/", "GET_GROUPID/", "", "", "", false, JsonConsults.JSON_CREATE_PERSONGROUP),
        CONSULT_CREATE_PERSON("POST",  "persongroups/", "GET_GROUPID/", "persons/", "", "", false, JsonConsults.JSON_CREATE_PERSON),
        CONSULT_ADD_FACE_2_PERSON("POST", "persongroups/", "GET_GROUPID/", "persons/", "GET_PERSONID/", "persistedFaces", false, JsonConsults.JSON_ADD_FACE_2_PERSON),
        CONSULT_TRAIN("POST", "persongroups/", "GET_GROUPID/", "train", "", "",false, JsonConsults.JSON_TRAIN),
        CONSULT_TRAINING("GET", "persongroups/", "GET_GROUPID/", "training", "", "", false,JsonConsults.JSON_TRAINING),
        CONSULT_DETECT_FACE("POST", "detect", "", "", "", "", true, JsonConsults.JSON_DETECT_FACE),
        CONSULT_IDENTIFY("POST", "identify", "", "", "", "", true, JsonConsults.JSON_IDENTIFY),
        CONSULT_GET_PERSON("GET", "persongroups/", "GET_GROUPID/", "persons/", "GET_PERSONID/", "", true,JsonConsults.JSON_EMPTY),
        CONSULT_GET_LIST_PERSONGROUPS("GET", "persongroups/", "", "", "", "", true, JsonConsults.JSON_EMPTY),
        CONSULT_GET_LIST_PERSONS("GET","persongroups/", "GET_GROUPID/", "persons/", "", "", true, JsonConsults.JSON_EMPTY),
        CONSULT_DELETE_PERSONGROUP("DELETE", "persongroups/", "GET_GROUPID/", "", "", "", false, JsonConsults.JSON_EMPTY),
        CONSULT_DELETE_PERSON("DELETE", "persongroups/", "GET_GROUPID/", "persons/", "GET_PERSONID/", "", false, JsonConsults.JSON_EMPTY),
        CONSULT_DELETE_FACE("DELETE", "persongroups/", "GET_GROUPID/", "persons/", "GET_PERSONID/", "persistedFaces/GET_PERSISTEDFACEID", false, JsonConsults.JSON_EMPTY);

        private val urlServidor = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/" //Servidor al que se realizan las peticiones de la API
        var httpConnection: HttpURLConnection? = null //Cliente HTTP para realizar las consultas
        private var outputStreamAsyncTask: OutputStreamAsyncTask? = null
        var delegate: AsyncResponse? = null
        var consulta: String
        private val consultaCopy: String

        init {
            consulta = obtenerUrlConsulta()
            consultaCopy = consulta
        }

        /**
         * Método que obtiene la consulta asociada a cada petición
         * @return Devuelve la consulta construida
         */
        private fun obtenerUrlConsulta(): String{
            return urlServidor + modulo + groupId + accion + personId + parametros
        }

        /**
         * Función que reemplaza los datos por defecto de la consulta HTTP por los necesarios para referirnos al GroupPerson o Person
         * @param data Objeto que contiene los datos a reemplazar
         */
        fun replaceData(data: Data) {
            if(this.consulta != this.consultaCopy)
                this.consulta = this.consultaCopy

            if (consulta.contains("GET_GROUPID"))
                consulta = consulta.replace("GET_GROUPID", data.groupPerson!!.id)

            if (consulta.contains("GET_PERSONID")) {
                if (this == CONSULT_GET_PERSON)
                    this.consulta = this.consulta.replace("GET_PERSONID", data.resultPersonId!!)
                else
                    consulta = consulta.replace("GET_PERSONID", data.person!!.personId!!)
            }

            if (consulta.contains("GET_PERSISTEDFACEID")) {
                consulta = consulta.replace("GET_PERSISTEDFACEID", data.person!!.persistedFaceId!!)
                consulta = consulta.replace("\"", "")
            }

            this.json.replaceData(data)
            initHttpRequest()
        }

        /**
         * Función que inicia el cliente HTTP
         */
        private fun initHttpRequest(){
            this.httpConnection = URL(this.consulta).openConnection() as HttpURLConnection
            this.httpConnection!!.doInput = true

            if((this.requestMethod != "GET") && (this.requestMethod != "DELETE"))
                this.httpConnection!!.doOutput = true

            this.httpConnection!!.requestMethod = this.requestMethod

            setHeader()
            setBody()
        }

        /**
         * Método que crea el header del JSON
         */
        private fun setHeader() {
            val parametrosHeader = this.json.header.split(",")
            val parametrosHeaderKey = this.json.headerKey.split(",")

            if(parametrosHeader.size > 1)
                this.httpConnection!!.setRequestProperty(parametrosHeader[0], parametrosHeader[1])
            this.httpConnection!!.setRequestProperty(parametrosHeaderKey[0], parametrosHeaderKey[1])
        }

        /**
         * Método que crea el body del JSON
         */
        private fun setBody(){
            this.outputStreamAsyncTask = OutputStreamAsyncTask()
            this.outputStreamAsyncTask!!.delegate = this.delegate
            this.outputStreamAsyncTask!!.execute(this)
        }
    }
}