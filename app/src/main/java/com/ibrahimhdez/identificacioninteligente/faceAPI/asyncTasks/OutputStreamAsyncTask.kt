package com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks

import android.graphics.Bitmap
import android.os.AsyncTask
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import java.io.*

/**
 * Tarea asíncrona para insertar en una consulta el body de un JSON
 * @author Ibrahim Hernández Jorge
 */
internal class OutputStreamAsyncTask: AsyncTask<Consultas.ApiConsults, Void, Boolean>() {
    var delegate: AsyncResponse? = null

    /**
     * Método que se ejecuta en background para añadir el JSON
     */
    override fun doInBackground(vararg consulta: Consultas.ApiConsults): Boolean {
        try {
            if ((consulta[0] == Consultas.ApiConsults.CONSULT_ADD_FACE_2_PERSON) || (consulta[0] == Consultas.ApiConsults.CONSULT_DETECT_FACE)) {
                val request = DataOutputStream(consulta[0].httpConnection!!.outputStream)
                val file = File(Comunicador.context!!.cacheDir, "fileName")
                file.createNewFile()

                val bos = ByteArrayOutputStream()
                Comunicador.data!!.bitMap!!.compress(Bitmap.CompressFormat.JPEG, 30, bos)
                val bitMapData = bos.toByteArray()

                val fos = FileOutputStream(file)
                fos.write(bitMapData)
                fos.flush()
                fos.close()
                val b = ByteArray(file.length().toInt())
                try {
                    val fileInputStream = FileInputStream(file)
                    fileInputStream.read(b)

                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (ioe: IOException) {
                    println("Error Reading The File.")
                    ioe.printStackTrace()
                }
                request.write(b)
            } else if ((consulta[0].requestMethod != "GET") && (consulta[0].requestMethod != "DELETE")) {
                val bw = BufferedWriter(OutputStreamWriter(consulta[0].httpConnection!!.outputStream, "UTF-8"))
                bw.write(consulta[0].json.body)
                bw.flush()
                bw.close()
            }
        } catch (e: Exception) {
            return false
        }
        return true
    }

    override fun onPostExecute(result: Boolean?) {
        if(result!!)
            this.delegate!!.processFinish(0)
        else
            this.delegate!!.processFailed()
    }
}
