package com.ibrahimhdez.identificacioninteligente.faceAPI

/**
 * Clase que maneja las excepciones de las peticiones HTTP
 * @author Ibrahim Hernández Jorge
 */
class Excepciones {
    companion object {
        /**
         * Método que gestiona las excepciones de las creaciones de un GroupPerson o un Person
         * @param response Número que devuelve la consulta HTTP
         * @param groupPerson Booleano que nos indica si estamos tratando un grupo (true) o una persona (false)
         * @return Respuesta de la excepción
         */
        fun createException(response: Int, groupPerson: Boolean): String? {
            var respuesta: String? = null

            when (response) {
                400 ->
                    respuesta = "Name is too long or invalid character in the Group ID."

                401 ->
                    respuesta = "Invalid subscription Key or user/plan is blocked."

                403 ->
                    respuesta = "Quota Exceeded"

                404 ->
                    respuesta = "An error occurred. Please, synchronize and try again!"

                409 -> {
                    respuesta = if(groupPerson)
                        "Person group already exists."
                    else
                        "The person group is still under training. Try again after training completed."
                }

                429 ->
                    respuesta = "Rate limit is exceeded. Try again in 26 seconds."

            }
            return respuesta
        }

        /**
         * Método que gestiona las excepciones de los añadidos de caras
         * @param response Número que devuelve la consulta HTTP
         * @return Respuesta de la excepción
         */
        fun addFaceException(response: Int): String? {
            var respuesta: String? = null

            when (response) {
                400 ->
                    respuesta = "No face detected in the image!"

                401 ->
                    respuesta = "Invalid subscription Key or user/plan is blocked."

                403 ->
                    respuesta = "Persisted face number reached limit, maximum is 248 per person."

                404 ->
                    respuesta = "Group or Person not found, please synchronize the groups."

                409 ->
                    respuesta = "Rate limit is exceeded. Try again in 26 seconds."

            }
            return respuesta
        }

        /**
         * Método que gestiona las excepciones de los borrados de personas y caras
         * @param response Número que devuelve la consulta HTTP
         * @return Respuesta de la excepción
         */
        fun deleteException(response: Int): String? {
            var respuesta: String? = null

            when (response) {
                401 ->
                    respuesta = "Invalid subscription Key or user/plan is blocked."

                403 ->
                    respuesta = "Out of call volume quota. Quota will be replenished in 2 days."

                404 ->
                    respuesta = "Group or Person not found, please synchronize the groups."

            }
            return respuesta
        }
    }
}