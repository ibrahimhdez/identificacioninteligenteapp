package com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks

import android.os.AsyncTask
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.net.UnknownHostException

/**
 * Clase para realizar las peticiones Web en background para evitar la congelación del hilo principal de nuestra aplicación
 * @author Ibrahim Hernández Jorge
 */
internal class ConnectionAsyncTask: AsyncTask<Consultas.ApiConsults, Void, Consultas.ApiConsults>() {
    var delegate: AsyncResponse? = null //Delegado que será la clase desde la que se llama a la tarea asíncrona
    private var failed: Boolean? = null

    /**
     * Método que hace la llamada y descarga en background la respuesta de la API
     * @param consulta Consulta a ejecutar
     * @return Número de respuesta HTTP
     */
    override fun doInBackground(vararg consulta: Consultas.ApiConsults): Consultas.ApiConsults {
        this.failed = false
        try {
            consulta[0].httpConnection!!.connect()
            val responseStream = BufferedInputStream(consulta[0].httpConnection!!.inputStream) //Hace la petición a la API
            val responseStreamReader = BufferedReader(InputStreamReader(responseStream)) //Recoge el resultado de la petición
            var line: String? = ""
            val stringBuilder = StringBuilder()
            val jsonString: String?

            //Bucle que recoge en una String la respuesta de la API
            while ({line = responseStreamReader.readLine(); line}() != null)
                stringBuilder.append(line).append("\n")

            responseStreamReader.close()
            jsonString = stringBuilder.toString()

            //Si el tipo de respuesta de la consulta es un JSON Array, lo recogemos
            if(consulta[0].respuestaJSONArray)
                Comunicador.controlador!!.arrayJsonString = JSONArray(jsonString)

            //Si la respuesta de la petición no es vacía y es distinta de las siguientes consultas, creamos un objeto JSON con el resultado para su posterior tratamiento
            else
                Comunicador.controlador!!.json = JSONObject(jsonString)

        } catch (e: UnknownHostException) {
            this.failed = true
        } catch (e: JSONException) {

        } catch (e: FileNotFoundException) {

        }
        return consulta[0]
    }

    /**
     * Función que llama al método processFinish de la clase que llamó a esta tarea asíncrona
     * @param consulta
     */
    override fun onPostExecute(consulta: Consultas.ApiConsults) {
        if (this.delegate != null) {
            when {
                this.failed!! -> delegate!!.processFailed()
                consulta == Consultas.ApiConsults.CONSULT_GET_LIST_PERSONGROUPS -> delegate!!.processFinish(consulta)
                else -> delegate!!.processFinish(consulta.httpConnection!!.responseCode)
            }
        }
    }
}


