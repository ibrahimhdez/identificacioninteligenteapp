package com.ibrahimhdez.identificacioninteligente.faceAPI

import android.content.Context
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.ConnectionAsyncTask
import com.ibrahimhdez.identificacioninteligente.utils.Data
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject

/**
 * Clase que controla todas las clases de la FaceAPI y con la que los demás paquetes del proyecto se comunican
 * @author Ibrahim Hernández Jorge
 */
class Controlador: AsyncResponse {
    var data: Data? = null
    private var mainAsyncResponse: AsyncResponse? = null
    private var cloudSynchronize: CloudSynchronize = CloudSynchronize()
    var json: JSONObject? = JSONObject()
    var arrayJsonString: JSONArray? = null
    var consulta: Consultas.ApiConsults? = null

    /**
     * Método que ejecuta una consulta
     * @param consulta Objeto con la consulta a ejecutar
     */
     fun execute(consulta: Consultas.ApiConsults, data: Data, asyncResponse: AsyncResponse) {
        //Preparamos los datos y delegados para ejecutar la consulta web
        this.data = data
        this.mainAsyncResponse = asyncResponse
        //Guardamos en el atributo de la clase la consulta API para ejecutarla cuando este preparada
        this.consulta = consulta
        //Ponemos como delegado de la consulta esta clase, así cuando termine la tarea asíncrona se ejecutará el método finishProcess de esta clase
        consulta.delegate = this
        //Reemplazamos los datos dados por la interfaz en la consulta, cuando termine se ejecutará el finishProcess
        consulta.replaceData(data)
    }

    /**
     * Método que se ejecuta cuando esta clase es la delegada de una tarea asíncrona y esta termina de ejecutarse
     * @param response Código de respuesta que devuelve la consulta HTTP
     */
    override fun processFinish(response: Int) {
        val asyncTask = ConnectionAsyncTask()
        //Asignamos la clase que invocó la acción como delegado de la clase que realiza las tareas asíncronas para cuando termine llame al método perteneciente
        asyncTask.delegate = this.mainAsyncResponse
        asyncTask.execute(this.consulta)
    }

    override fun processFailed() {
        this.mainAsyncResponse!!.processFailed()
    }

    /**
     * Método para limpiar el atributo JSON una vez sea usado
     * @param isArray Estará a true si tenemos que borrar el JSONArray y a false si lo que hay que borrar es el JSON
     */
    fun cleanJSON(isArray: Boolean) {
        //Si estamos tratando un JSONArray
        if(isArray)
            this.arrayJsonString = JSONArray("[]")
        //Si estamos tratando un JSONObject
        else
            this.json = JSONObject("{}")
    }

    /**
     * Método que realiza una sincronización con la nube
     * @param progress AlertDialog con el progreso de la sincronización
     * @param context Contexto para mostrar un Toast al usuario en caso de error
     */
    fun cloudSynchronization(progress: SpotsDialog, context: Context) {
        this.cloudSynchronize.obtainGroups(Comunicador.controlador!!.arrayJsonString!!, progress, context)
    }
}
