package com.ibrahimhdez.identificacioninteligente

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.`interface`.MainActivity
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import dmax.dialog.SpotsDialog
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.hardware.Camera.PictureCallback
import java.io.*
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.MediaRecorder
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.View
import android.widget.EditText
import com.ibrahimhdez.identificacioninteligente.`interface`.groupPerson.ActionsGroupPersonActivity
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.faceAPI.Controlador
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.preferences.Preferencias
import com.ibrahimhdez.identificacioninteligente.serverBBDD.ServerManager
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.SpeakerConsults
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.VoiceController
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import com.ibrahimhdez.identificacioninteligente.utils.Data
import org.json.JSONObject
import pl.droidsonroids.gif.GifImageView

class IdentifyActivity: AppCompatActivity(), AsyncResponse {
    private var camera: Camera? = null
    private var faceIdButton: Button? = null
    private var loginButton: Button? = null
    private var voiceButton: Button? = null
    private var progress: AlertDialog? = null
    private var detect: Boolean = false
    private var voiceLogin: Boolean = false
    private var noProgress: Boolean = false
    private var userEditText: EditText? = null
    private var passwordEditText: EditText? = null
    private var gif: GifImageView? = null
    private var mediaRecorder: MediaRecorder? = null
    private var recording = false

    companion object {
        const val PICK_IMAGE_REQUEST = 1
        const val TAKE_PICTURE = 2
        const val RECORD_AUDIO = 3
        const val EXTERNAL_STORAGE = 4
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.identify_activity)

        //Pedimos permisos para grabar con micrófono y para escribir en el almacenamiento
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO)
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_STORAGE)

        //Iniciamos los componentes de la activity
        iniciarComponentes()
        //Obtenemos el grupo de las preferencias y si no hay obligamos a que se seleccione uno
        getPreferencesGroup()
    }

    /**
     * Nétodo para iniciar los componentes de la clase
     */
    private fun iniciarComponentes() {
        //Inicialización de los objetos generales para toda la aplicación
        Comunicador.controlador = Controlador()
        Comunicador.voiceController = VoiceController()
        Comunicador.serverManager = ServerManager()
        Comunicador.data = Data()
        Comunicador.context = this
        DBComunicator.initInstance(this)

        //Inicialización de los elementos del activity
        this.progress = SpotsDialog(this, "Log In...", R.style.spotDialogStyle)
        this.progress!!.setCancelable(true)
        this.progress!!.setCanceledOnTouchOutside(false)
        this.gif = findViewById(R.id.gif)
        this.gif!!.bringToFront()
        //Si venimos de un cierre de sesión, no enseñamos el gif de escaneo de cara
        if(intent.hasExtra("LOGOUT"))
            dismissFaceId()

        this.userEditText = findViewById(R.id.userEditText)
        this.passwordEditText = findViewById(R.id.passwordEditText)

        this.loginButton = findViewById(R.id.logInButton)
        this.loginButton!!.setOnClickListener({
            login()
        })

        this.faceIdButton = findViewById(R.id.FaceLogInButton)
        this.faceIdButton!!.setOnClickListener({
            faceId()
        })

        this.voiceButton = findViewById(R.id.voiceLogInButton)
        this.voiceButton!!.setOnClickListener({
            voiceLogin()
        })
    }

    /**
     * Método que se ejecuta al iniciar la aplicación, obtiene el grupo actual de las preferencias si existe, si no
     * salta al activity de grupos para seleccionar o crear uno
     */
    private fun getPreferencesGroup() {
        val groupId = Preferencias.getStringPreferencias(this, Preferencias.group)
        //Obtenemos la fila del grupo de las preferencias
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_GROUPS} WHERE ${DBContract.DBEntry.ID} = '$groupId'")
        cursor.moveToFirst()
        //Si el tamaño del cursor es mayor a 0, quiere decir que ya hay un grupo en las preferencias, por lo que obtenemos sus datos
        if(cursor.count > 0) {
            Comunicador.data!!.groupPerson = GroupPerson(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)))
            //Si no venimos de hacer un logout, realizamos una identifación de cara
            if(!intent.hasExtra("LOGOUT"))
                faceId()
        }
        //Si el tamaño es igual a 0, saltamos al activity de grupos, ahí podremos seleccionar o crear uno
        else
            startActivity(Intent(this, ActionsGroupPersonActivity::class.java))
    }

    /**
     * Método que realiza un login tradicional con usuario y contraseña
     */
    private fun login() {
        //Consultamos en la tabla USERS si existe un usuario que cumpla que el user y password que se introduce mediante los edit text del layout
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_USERS} WHERE ${DBContract.DBEntry.USER} = '${this.userEditText!!.text}' AND ${DBContract.DBEntry.PASSWORD} = '${this.passwordEditText!!.text}'")
        //Si existe alguna entrada, el login es satisfactorio
        if(cursor.moveToFirst()) {
            //Obtenemos la persona asociada al usuario logueado
            val cursorPerson = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_PERSONS} WHERE ${DBContract.DBEntry.ID} = '${cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.PERSON_ID))}'")
            cursorPerson.moveToFirst()
            this.progress!!.show()
            Comunicador.data!!.person = Person(cursorPerson.getString(cursorPerson.getColumnIndex(DBContract.DBEntry.ID)),
                    cursorPerson.getString(cursorPerson.getColumnIndex(DBContract.DBEntry.NAME)))
            //Después de 1,5 segundos, ejecuta su contenido
            Handler().postDelayed({
                this.progress!!.dismiss()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }, 1500)
        }
        //Si el login no es satisfactorio, mostrar un mensaje de error al usuario
        else
            Toast.makeText(this, "Incorrect user o password. Try again!", Toast.LENGTH_LONG).show()
    }

    /**
     * Método que realiza el logueo por cara
     */
    private fun faceId() {
        showFaceId() //Muestra el gif de escaneo de cara
        takeSnapShots() //Saca una foto para identificar al usuario
        //Pone los botones del activity como invisibles para una mejor visualización
        changeButtonsVisibility(View.INVISIBLE)
        //Después de 1,5 segundos, ejecuta su contenido
        Handler().postDelayed({
            dismissFaceId() //Deja de mostrar el gif de identificación de cara
            //Muestra el dialog de progreso si se ha detectado una cara y se procede a loguear al usuario
            if(!this.noProgress)
                this.progress?.show()
            this.noProgress = false
            //Vuelve a mostrar los botones del activity
            changeButtonsVisibility(View.VISIBLE)
        }, 1500)
    }

    private fun voiceLogin() {
        if (this.recording) {
            this.recording = false
            this.mediaRecorder!!.stop()
            this.mediaRecorder!!.reset()
            this.mediaRecorder!!.release()
            voiceIdentify()
        } else {
            this.mediaRecorder = MediaRecorder()
            this.mediaRecorder!!.reset()
            this.mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
            this.mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            this.mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            val destination = "/sdcard/test.wav"
            this.mediaRecorder!!.setOutputFile(destination)
            this.mediaRecorder!!.prepare()
            this.mediaRecorder!!.start()
            this.recording = true
        }
    }

    private fun voiceIdentify() {
        Comunicador.data!!.profiles = getProfilesId()
        if(Comunicador.data!!.profiles!!.size > 0) {
            this.voiceLogin = true
            Comunicador.voiceController!!.execute(SpeakerConsults.ApiConsults.CALL_IDENTIFY, Comunicador.data!!, this)
        } else
            Toast.makeText(this, "This group hasn´t have voice profiles!", Toast.LENGTH_LONG).show()
    }

    /**
     * Método para obtener todos los ID de los perfiles de voz de las personas del grupo
     * @return Array con los IDs de los perfiles de voz del grupo actual
     */
    private fun getProfilesId(): ArrayList<String> {
        val profiles = ArrayList<String>()
        var profile: String

        //Consulta para obtener los IDs de perfiles de voz de las personas del grupo actual
        val cursor = DBComunicator.getInstance().rawConsult("SELECT ${DBContract.DBEntry.VOICE_ID} " +
                "FROM ${DBContract.DBEntry.TABLA_PERSONS} " +
                "WHERE ${DBContract.DBEntry.GROUP_ID} = '${Comunicador.data!!.groupPerson!!.id}'")

        //Si hay algún resultado pasamos a insertar los IDs en el array
        if(cursor.count > 0) {
            //Recorremos el cursor
            while(cursor.moveToNext()) {
                //Obtenemos el ID actual
                profile = cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.VOICE_ID))
                //Si la persona tiene perfil de voz, lo añadimos al array que devolveremos
                if (profile != "")
                    profiles.add(profile)
            }
        }
        //Si el tamaño del cursor es 0 quiere decir que no hay personas en el grupo actual
        else
            Toast.makeText(this, "First insert a person in this group!", Toast.LENGTH_LONG).show()

        return profiles
    }

    /**
     * Método para inflar un menú
     * @param menu Menu que inflaremos
     * @return Booleano con el resultado de la operación
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método que se ejecuta al volver al activity.
     * Su función es desaparecer el dialog de progreso si está en funcionamiento
     */
    override fun onResume() {
        super.onResume()
        this.progress!!.dismiss()
    }

    /**
     * Método que cambia la visibilidad de los botones del Activity
     * @param view Int que indica si los botones cambian a visibles o invisibles
     */
    private fun changeButtonsVisibility(view: Int) {
        this.loginButton!!.visibility = view
        this.faceIdButton!!.visibility = view
        this.voiceButton!!.visibility = view
    }

    /**
     * Método que muestra el gif de la identificación de cara
     */
    private fun showFaceId() {
        this.gif!!.setFreezesAnimation(true)
        this.gif!!.visibility = View.VISIBLE
    }

    /**
     * Método que deja de mostrar el gif de la identificación de cara
     */
    private fun dismissFaceId() {
        this.gif!!.visibility = View.INVISIBLE
    }

    /**
     * Método para sacar una foto.
     * Comprueba que tenemos los permisos de cámara, si no los tenemos, los pedimos, si los tenemos, sacamos la foto
     */
    private fun takeSnapShots() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.CAMERA),
                        TAKE_PICTURE)
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else
            abrirCamara()
    }

    //Variable que contiene un bloque que saca una foto y detecta la cara de esta foto
    private var cameraCallback: PictureCallback = PictureCallback { data, camera ->
        camera!!.stopPreview()
        camera.release()
        //Sacamos el bitmap de la foto sacada y corregimos el ángulo de rotación de la cámara
        Comunicador.data!!.bitMap = BitmapFactory.decodeByteArray(data, 0, data.size).rotate(270.toFloat())
        this.detect = true

        //Si ya tenemos un grupo actual, detectamos la cara
        if(Comunicador.data!!.groupPerson != null)
            Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_DETECT_FACE, Comunicador.data!!, this)
        //Si no lo tenemos, oblgamos al usuario a crear o seleccionar uno
        else {
            this.progress!!.dismiss()
            this.noProgress = true //Evitamos que se muestre el Dialog de progreso posteriormente
            Toast.makeText(this, "No groups found, synchronize or create one!", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método que abre la cámara y saca una foto
     */
    private fun abrirCamara() {
        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT)
        camera!!.setPreviewTexture(SurfaceTexture(0))
        camera!!.startPreview()
        try {
            Handler().postDelayed({ camera!!.takePicture(null, null, cameraCallback) }, 300)
        }catch (e: Exception) {

        }
    }

    /**
     * Método para rotar un bitmap
     * @param degrees Grados que queremos girar el bitmap
     * @return Bitmap con el resultado de la rotación
     */
    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    /**
     * Método que es ejecutado una vez elegida una foto de la galería y trata el resultado
     * @param requestCode Código con el que se hace la petición, con el sabremos de que acción es llamado
     * @param resultCode Código del resultado que indica que la operación haya ido OK
     */
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Si se ha llamado desde el picker de imagen y se ha seleccionado una imagen
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri = data.data //Obtenemos los datos ressultado
            try {
                Comunicador.data!!.bitMap = MediaStore.Images.Media.getBitmap(contentResolver, uri) //Añadimos la imagen seleccionada al Bitmap actual
                this.detect = true
                Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_DETECT_FACE, Comunicador.data!!, this) //Intentamos detectar una cara en la imagen seleccionada

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Método que se ejecuta cuando se termina una tarea asíncrona y esta clase es la delegada
     * @param response Número de respuesta que devuelve la petición HTTP
     */
    override fun processFinish(response: Int) {
        //Si estamos detectando una cara
        if(detect) {
            this.detect = false
            //Si una cara es detectada
            if(Comunicador.controlador!!.arrayJsonString!!.length() > 0) {
                Comunicador.data!!.faceIds = (Comunicador.controlador!!.arrayJsonString!!.get(0) as JSONObject).getString("faceId")
                Comunicador.controlador!!.cleanJSON(true)
                //Preparamos e identificamos la cara que hemos encontrado
                Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_IDENTIFY, Comunicador.data!!, this)
            }
            //Si no detectamos cara en la imagen, avisamos al usuario
            else {
                this.progress?.dismiss()
                this.noProgress = true
                Toast.makeText(this, "No face detected!", Toast.LENGTH_LONG).show()
            }
        }
        else if(this.voiceLogin) {
            this.voiceLogin = false

        }
        //Si estamos identificando a una persona con una cara previamente detectada
        else {
            this.progress!!.dismiss()
            //Si no ha ocurrido ningún error relacionado con una aplicación no sincronizada con la nube
            if (Comunicador.controlador!!.arrayJsonString!!.length() > 0) {
                val jsonArray = Comunicador.controlador!!.arrayJsonString!!.getJSONObject(0).getJSONArray("candidates")
                Comunicador.controlador!!.cleanJSON(true)
                //Si hemos identificado una persona con la cara actual
                if (jsonArray.length() > 0) {
                    val json = jsonArray.getJSONObject(0)
                    //Buscamos en la base de datos local la persona identificada
                    val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_PERSONS, arrayOf(DBContract.DBEntry.ID), arrayOf(json.getString("personId")))
                    //Si identificamos a una persona y esta se encuentra en la base de datos local
                    if(cursor.count > 0) {
                        cursor.moveToFirst()
                        Comunicador.data!!.person = Person(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)),
                                cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)))
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                    //Si identificamos a una persona pero esta no se encuentra en la base de datos local, quiere decir que no tenemos
                    //una versión de la aplicación sincronizada
                    else {
                        finish()
                        Toast.makeText(this, "Groups not sinchronized, please synchronize and try again!", Toast.LENGTH_LONG).show()
                    }
                }
                //Si no hemos idenfiticado una persona, quiere decir que esta no está registrada
                else
                    Toast.makeText(this, "No register person!", Toast.LENGTH_LONG).show()
            }
            else
                Toast.makeText(this, "An error occurred, please synchronize and try again!", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.noProgress = true
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, ActionsGroupPersonActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}