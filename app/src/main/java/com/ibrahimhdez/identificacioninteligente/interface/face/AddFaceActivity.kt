package com.ibrahimhdez.identificacioninteligente.`interface`.face

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.ibrahimhdez.identificacioninteligente.R
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.faceAPI.Excepciones
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog
import java.io.IOException
import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.view.View
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse

/**
 * Activity para el añadido de caras a personas
 * @author Ibrahim Hernández Jorge
 */
class AddFaceActivity: AppCompatActivity(), AsyncResponse {
    private var nameFaceEditText: EditText? = null
    private var elegirImagenBoton: Button? = null
    private var progress: AlertDialog? = null
    private var imagenElegida: Boolean = false
    private var faceImage: ImageView? = null
    private var imageUri: Uri? = null

    companion object {
        const val SELECT_IMAGE = 1
        const val TAKE_PICTURE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_face_activity)

        iniciarComponentes()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun iniciarComponentes() {
        this.nameFaceEditText = findViewById(R.id.faceNameEditText)
        this.elegirImagenBoton = findViewById(R.id.elegirFotoBoton)
        this.faceImage = findViewById(R.id.faceView)
        this.faceImage!!.visibility = View.INVISIBLE
        this.progress = SpotsDialog(this, "Adding Face...", R.style.spotDialogStyle)
        this.progress!!.setCancelable(true)
        this.progress!!.setCanceledOnTouchOutside(false)

        this.elegirImagenBoton!!.setOnClickListener({ _ ->
            dialogPhoto()
        })

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun camposVacios(): Boolean {
        return (this.nameFaceEditText!!.text.isEmpty() || !this.imagenElegida)
    }

    private fun dialogPhoto() {
        try {
            val items = arrayOf<CharSequence>("Choose from gallery", "Take a picture")

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Select method:")
            builder.setItems(items) { _, item ->
                when (item) {
                    0 -> {
                        val intent = Intent()
                        intent.type = "image/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
                    }
                    1 -> {//startActivityForResult(Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE), TAKE_PICTURE)
                        if (ContextCompat.checkSelfPermission(this,
                                        Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                            Manifest.permission.CAMERA)) {

                                // Show an expanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(this,
                                        arrayOf(Manifest.permission.CAMERA),
                                        TAKE_PICTURE)

                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }
                        else
                            abrirCamara()
                    }
                }
            }
            val alert = builder.create()
            alert.show()
        } catch (e: Exception) {
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            TAKE_PICTURE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    abrirCamara()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    private fun abrirCamara() {
        if(!checkExternalStoragePermission())
            return

        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "MyPicture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Photo taken on " + System.currentTimeMillis())
        this.imageUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(intent, TAKE_PICTURE)
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_add -> {
                if(!camposVacios()) {
                    this.progress!!.show()
                    Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_ADD_FACE_2_PERSON, Comunicador.data!!, this)
                }
                else
                    Toast.makeText(this, "Fill the fields", Toast.LENGTH_LONG).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if(requestCode == SELECT_IMAGE || requestCode == TAKE_PICTURE) {
                if(requestCode == TAKE_PICTURE) {
                    val thumbnail = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                    val ei = ExifInterface(getRealPathFromURI(imageUri!!))
                    val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
                    var rotatedBitmap: Bitmap? = null

                    when(orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 ->
                            rotatedBitmap = thumbnail.rotate(90.toFloat())

                        ExifInterface.ORIENTATION_ROTATE_180 ->
                            rotatedBitmap = thumbnail.rotate(180.toFloat())

                        ExifInterface.ORIENTATION_ROTATE_270 ->
                            rotatedBitmap = thumbnail.rotate(270.toFloat())

                        ExifInterface.ORIENTATION_NORMAL ->
                            rotatedBitmap = thumbnail
                    }
                    Comunicador.data!!.bitMap = rotatedBitmap
                    contentResolver.delete(imageUri!!, null, null)
                }
                else {
                    val uri = data!!.data
                    try {
                        Comunicador.data!!.bitMap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                this.imagenElegida = true
                this.elegirImagenBoton!!.text = "Choose Other Image"
                this.faceImage!!.setImageBitmap(Comunicador.data!!.bitMap)
                this.faceImage!!.visibility = View.VISIBLE
            }
        }
    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    private fun checkExternalStoragePermission(): Boolean {
        val permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 225)
        else
            return true

        return false
    }

    private fun getRealPathFromURI(contentURI: Uri): String {
        val result: String
        val cursor = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) {
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    override fun processFinish(response: Int) {
        this.progress!!.dismiss()
        if(response != Comunicador.successfulCall) {
            if(response != 202)
                Toast.makeText(baseContext, Excepciones.addFaceException(response), Toast.LENGTH_LONG).show()
        }
        else {
            if (Comunicador.controlador!!.json!!.has("persistedFaceId")) {
                this.imagenElegida = false
                Toast.makeText(baseContext, "Add face successful!", Toast.LENGTH_LONG).show()
                Comunicador.data!!.person!!.persistedFaceId = Comunicador.controlador!!.json!!.getString("persistedFaceId")
                Comunicador.controlador!!.cleanJSON(false)
                DBComunicator.getInstance().insertFace(this.nameFaceEditText!!.text.toString(), Comunicador.data!!.person!!)
                Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_TRAIN, Comunicador.data!!, this)
                finish()
            }
            else
                Toast.makeText(this, "An error occurred, please try again.", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }
}