package com.ibrahimhdez.identificacioninteligente.`interface`

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.ibrahimhdez.identificacioninteligente.R
import kotlinx.android.synthetic.main.activity_calendar.*

class CalendarActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        //Habilitar botón hacia atrás en toolbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun markCalendar() {

    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}