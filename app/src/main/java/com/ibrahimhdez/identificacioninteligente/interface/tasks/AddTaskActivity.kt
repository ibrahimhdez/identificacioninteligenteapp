package com.ibrahimhdez.identificacioninteligente.`interface`.tasks

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils.isEmpty
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import kotlinx.android.synthetic.main.add_task_activity.*

class AddTaskActivity: AppCompatActivity() {
    private var descriptionEditText: EditText? = null
    private var datePicker: DatePicker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_task_activity)

        iniciarComponentes()
    }

    /**
     * Método que inicia los componentes del Activity
     */
    private fun iniciarComponentes() {
        this.descriptionEditText = task_description
        this.datePicker = findViewById(R.id.datePicker)
        //Habilitar botón hacia atrás en toolbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_add -> {
                //Comprobamos si los campos obligatorios están vacíos
                if(camposVacios())
                    Toast.makeText(baseContext, "Fill description field!", Toast.LENGTH_LONG).show()
                //Si no lo están, insertamos la tarea en la base de datos
                else {
                    DBComunicator.getInstance().insertTask(this.descriptionEditText!!.text.toString(),
                            "${this.datePicker!!.dayOfMonth}/${this.datePicker!!.month + 1}/${this.datePicker!!.year}", Comunicador.data!!.person!!)
                    println(DBComunicator.getInstance().getTableAsString(DBContract.DBEntry.TABLA_TASKS))
                    finish()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método que comprueba si los campos obligatorios del activity están rellenos
     */
    private fun camposVacios(): Boolean {
        return isEmpty(this.descriptionEditText!!.text)
    }
}