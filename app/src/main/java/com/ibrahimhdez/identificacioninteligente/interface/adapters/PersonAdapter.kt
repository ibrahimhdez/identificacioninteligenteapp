package com.ibrahimhdez.identificacioninteligente.`interface`.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.`interface`.person.PersonDelegate
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import java.util.ArrayList

class PersonAdapter(persons: ArrayList<Person>) : RecyclerView.Adapter<PersonAdapter.ViewHolderPerson>() {
    var persons: ArrayList<Person>? = persons
    var delegate: PersonDelegate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPerson {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_person, parent, false)

        return ViewHolderPerson(view)
    }

    override fun onBindViewHolder(holder: ViewHolderPerson, position: Int) {
        holder.namePerson.text = this.persons!![position].name
        holder.personId = this.persons!![position].personId
        mostrarCara(holder)

        delegate!!.processFinish(holder.menu, holder)
    }

    private fun mostrarCara(holder: ViewHolderPerson) {
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM PERSONS, FACES WHERE (PERSONS.ID = '${holder.personId}') AND (PERSONS.ID = FACES.PERSON_ID)")

        if(cursor.count == 0)
            holder.faceImageView.visibility = View.INVISIBLE
    }

    override fun getItemCount(): Int {
        return this.persons!!.size
    }

    inner class ViewHolderPerson(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var namePerson: TextView = itemView.findViewById(R.id.name_person)
        var menu: ImageView = itemView.findViewById(R.id.menu_imageview)
        var faceImageView: ImageView = itemView.findViewById(R.id.faceImageView)
        var personId: String? = null
    }
}
