package com.ibrahimhdez.identificacioninteligente.`interface`.person

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.getbase.floatingactionbutton.FloatingActionButton
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.ibrahimhdez.identificacioninteligente.faceAPI.Excepciones
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.`interface`.adapters.PersonAdapter
import com.ibrahimhdez.identificacioninteligente.`interface`.face.AddFaceActivity
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.`interface`.voice.AddVoiceActivity
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.serverBBDD.ServerManager
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.action_person_activity.*

class ActionPersonActivity : AppCompatActivity(), PersonDelegate, AsyncResponse {
    private var persons: ArrayList<Person>? = null
    private var recyclerPersons: RecyclerView? = null
    private var adapter: PersonAdapter? = null
    private var fabMenu: FloatingActionsMenu? = null
    private var fabCreatePerson: FloatingActionButton? = null
    private var fabUploadNube: FloatingActionButton? = null
    private var progress: AlertDialog? = null
    private var progressDeletingFace: AlertDialog? = null
    private val selectedFaces = ArrayList<String>()
    private var deletingFaces: Boolean = false
    private var scaledUp: Animation? = null
    private var scaledDown: Animation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.action_person_activity)
        this.title = "Persons of the group"

        iniciarComponentes()
    }

    override fun onRestart() {
        super.onRestart()
        actualizarRecycler()
    }

    /**
     * Método que inicia los componentes del Activity
     */
    private fun iniciarComponentes() {
        this.scaledUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
        this.scaledDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
        this.progress = SpotsDialog(this, "Deleting person...", R.style.spotDialogStyle)
        this.progress!!.setCancelable(true)
        this.progress!!.setCanceledOnTouchOutside(false)
        this.progressDeletingFace = SpotsDialog(this, "Deleting face...", R.style.spotDialogStyle)
        this.progressDeletingFace!!.setCancelable(true)
        this.progressDeletingFace!!.setCanceledOnTouchOutside(false)
        this.recyclerPersons = findViewById(R.id.recyclerViewPerson)
        this.recyclerPersons!!.layoutManager = LinearLayoutManager(this)

        iniciarFloatingButton()
        actualizarRecycler()

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun iniciarFloatingButton() {
        this.fabMenu = findViewById(R.id.menu_fab_create_person)
        this.fabCreatePerson = findViewById(R.id.accion_create_person)
        this.fabCreatePerson!!.setOnClickListener({ _ ->
            this.fabMenu!!.collapse()
            val intent = Intent(applicationContext, CreatePersonActivity::class.java)
            startActivity(intent)
        })

        this.fabUploadNube = accion_upload_cloud
        this.fabUploadNube!!.setOnClickListener({ _ ->
            this.fabMenu!!.collapse()
            val cursor = DBComunicator.getInstance().rawConsult("SELECT ${DBContract.DBEntry.USER} " +
                    "FROM ${DBContract.DBEntry.TABLA_USERS} " +
                    "WHERE ${DBContract.DBEntry.GROUP_ID} = '${Comunicador.data!!.groupPerson!!.id}' " +
                    "AND ${DBContract.DBEntry.PASSWORD} = '${Comunicador.defaultPassword}'")
            val serverManager = ServerManager()

            serverManager.getPasswords(cursor)
        })

        this.recyclerPersons!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && fabMenu!!.visibility == View.VISIBLE) {
                    fabMenu!!.startAnimation(scaledDown)
                    fabMenu!!.visibility = View.INVISIBLE
                }
                else if (dy < 0 && fabMenu!!.visibility != View.VISIBLE) {
                    fabMenu!!.startAnimation(scaledUp)
                    fabMenu!!.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun actualizarRecycler() {
        this.persons = obtainPersons()
        //Obtener el adapter de las Person para añadírselo al RecyclerView de los grupos
        this.adapter = PersonAdapter(this.persons!!)
        this.adapter!!.delegate = this
        this.recyclerPersons!!.adapter = adapter
    }

    /**
    * Método para tratar el item seleccionado de la interfaz
    * @param item Item que ha sido pulsado
    * @return Devuelve Boolean que contendrá el resultado de la acción
    */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método que obtiene de la Base de Datos los GroupPersons vigentes
     * @return ArrayList con todos los GroupPersons alojados en la Base de Datos local
     */
    private fun obtainPersons(): ArrayList<Person>? {
        val arrayPersons = ArrayList<Person>()
        //Cursor que contiene los resultados de la llamada a la tabla "PERSONS"
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_PERSONS, arrayOf(DBContract.DBEntry.GROUP_ID), arrayOf(Comunicador.data!!.groupPerson!!.id))

        //Bucle que va sacando del cursor todas las Persons
        while (cursor.moveToNext()) {
            arrayPersons.add(Person(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME))))
        }
        cursor.close()

        return arrayPersons
    }

    /**
     * Método que obtiene una cara de la base de datos y la borra tanto en local como en nube
     */
    private fun deleteFaces() {
        //Obtenemos de la base de datos, la cara seleccionada en primer lugar
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_FACES, arrayOf(DBContract.DBEntry.NAME), arrayOf(this.selectedFaces[0]))
        cursor.moveToFirst()
        this.selectedFaces.removeAt(0) //Borramos esta cara de las seleccionadas porque ya la obtuvimos
        this.deletingFaces = true //Preparamos la interface para avisar de que es un borrado de caras y se hagan las acciones pertinentes en el finishProcess()
        Comunicador.data!!.person!!.persistedFaceId = cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID))
        DBComunicator.getInstance().deleteFaceByName(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)), Comunicador.data!!.person!!) //Borramos la cara de local
        Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_DELETE_FACE, Comunicador.data!!, this) //Borramos la cara de la nube
    }

    override fun processFinish(response: Int) {
        if(response == Comunicador.successfulCall) {
            //Si no estamos borrando caras, sino personas, entramos en este if
            if (!this.deletingFaces) {
                if (response != Comunicador.successfulCall) {
                    //Toast.makeText(baseContext, Excepciones.createException(response, true), Toast.LENGTH_LONG).show()
                }
                else {
                    DBComunicator.getInstance().deletePerson(Comunicador.data!!.person!!)
                    actualizarRecycler()
                    Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_TRAIN, Comunicador.data!!, this)
                }
            }
            //Si estamos borrando caras
            else {
                //Si todavía quedan caras seleccionadas, seguimos borrando
                if (this.selectedFaces.isNotEmpty())
                    deleteFaces()
                //Si ya borramos todas las caras, emitimos un aviso y actualizamos recycler
                else {
                    actualizarRecycler()
                    Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_TRAIN, Comunicador.data!!, this)
                }
            }
        }
        else if(response == 202) {
            if(!this.deletingFaces) {
                this.progress!!.dismiss()
                if(this.fabMenu!!.visibility == View.INVISIBLE) {
                    fabMenu!!.startAnimation(scaledUp)
                    fabMenu!!.visibility = View.VISIBLE
                }
                Toast.makeText(baseContext, "Person deleted!", Toast.LENGTH_LONG).show()
            }
            else {
                this.progressDeletingFace!!.dismiss()
                Toast.makeText(this, "Faces deleted!", Toast.LENGTH_LONG).show()
                this.deletingFaces = false //Ponemos a false para avisar a la interface de que ya no estamos borrando caras
            }
        }

        else {
            this.progress!!.dismiss()
            this.progressDeletingFace!!.dismiss()
            Toast.makeText(this, Excepciones.deleteException(response), Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método que obtiene las caras de una persona
     * @param person Persona de la que obtendremos las caras
     * @return Array de Strings con los nombres de las caras para ser mostrados en el AlertDialog de borrado de caras
     */
    private fun getFaces(person: Person): Array<String>{
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_FACES, arrayOf(DBContract.DBEntry.PERSON_ID), arrayOf(person.personId!!))
        val caras = mutableListOf<String>()

        while(cursor.moveToNext())
            caras.add(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME)))

        return caras.toTypedArray()
    }

    override fun processFinish(menu: ImageView, holder: PersonAdapter.ViewHolderPerson) {
        menu.setOnClickListener({
            val popupMenu = PopupMenu(menu.context, menu)
            popupMenu.menuInflater.inflate(R.menu.person_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener({ item: MenuItem? ->
                Comunicador.data!!.person = Person(holder.personId!!, holder.namePerson.text.toString()) //Añadimos la persona que pulsamos como la actual

                when (item!!.itemId) {
                    R.id.add_face -> startActivity(Intent(applicationContext, AddFaceActivity::class.java))


                    R.id.add_voice -> startActivity(Intent(applicationContext, AddVoiceActivity::class.java))

                    R.id.delete_face -> {
                        //Creamos el alertDialog para seleccionar las caras que queremos borrar
                        val alertDialog: AlertDialog
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle("Select Faces for Delete")
                        val values = getFaces(Comunicador.data!!.person!!)
                        //Si la persona tiene caras, continuamos con el proceso
                        if(values.isNotEmpty()) {
                            builder.setMultiChoiceItems(values, null, { _, item, isChecked ->
                                //Si la opción está seleccionada, la añadimos a las caras seleccionadas
                                if (isChecked)
                                    this.selectedFaces.add(values[item])
                                //Si la opción se desmarca, se elimina de las caras seleccionadas
                                else
                                    this.selectedFaces.remove(values[item])
                            }).setPositiveButton("Delete", { _, _ ->
                                //Si al darle al botón de borrar caras, hay caras seleccionadas, proseguimos con su eliminación
                                if(this.selectedFaces.isNotEmpty()) {
                                    this.progressDeletingFace!!.show()
                                    deleteFaces()
                                }
                            })
                            alertDialog = builder.create()
                            alertDialog.show()
                        }
                        //Si la persona no tiene caras, mostrar un Toast avisando
                        else
                            Toast.makeText(this, "This person has no faces!", Toast.LENGTH_LONG).show()
                    }

                    R.id.delete_person -> {
                        this.progress!!.show()
                        Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_DELETE_PERSON, Comunicador.data!!, this)
                    }
                }
                true
            })
            popupMenu.show()
        })
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }
}