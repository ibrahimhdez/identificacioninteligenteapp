package com.ibrahimhdez.identificacioninteligente.`interface`.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Tasks
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import kotlinx.android.synthetic.main.card_tasks.view.*
import java.util.ArrayList

class TaskAdapter(tasks: ArrayList<Tasks>) : RecyclerView.Adapter<TaskAdapter.ViewHolderTask>() {
    private var tasks: ArrayList<Tasks>? = tasks
    var refreshRecycler: (() -> Unit)? = null
    var delegate: AsyncResponse? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTask {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_tasks, parent, false)

        return ViewHolderTask(view)
    }

    override fun onBindViewHolder(holder: ViewHolderTask, position: Int) {
        holder.taskId = this.tasks!![position].id
        holder.descriptionTask.text = this.tasks!![position].descripcion
        holder.dateTask.text = "Finish date: ${this.tasks!![position].fecha}"
        mostrarCheck(holder)

        //Iniciamos menú de tres punto
        initMenu(holder.menu, holder)
    }

    private fun initMenu(menu: ImageView, holder: ViewHolderTask) {
        menu.setOnClickListener({
            val popupMenu = PopupMenu(menu.context, menu)
            popupMenu.menuInflater.inflate(R.menu.task_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener({ item: MenuItem? ->
                Comunicador.data!!.task = Tasks(holder.taskId!!, holder.descriptionTask.text as String, holder.dateTask.text as String)

                when (item!!.itemId) {
                    R.id.task_done -> {
                        DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_TASKS, DBContract.DBEntry.DONE, 1.toString(), DBContract.DBEntry.ID, Comunicador.data!!.task!!.id)
                        this.refreshRecycler?.invoke()
                    }

                    R.id.delete_task -> {
                        DBComunicator.getInstance().deleteTask(Comunicador.data!!.task!!)
                        this.delegate?.processFinish(1)
                    }
                }
                true
            })
            popupMenu.show()
        })
    }

    /**
     * Método para mostrar el check de una tarjeta de tareas cuando está hecha
     * @param holder Holder que mostraá u ocultará el check
     */
    private fun mostrarCheck(holder: ViewHolderTask) {
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_TASKS} WHERE ${DBContract.DBEntry.ID} = ${holder.taskId}")

        if(cursor.moveToFirst() && cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.DONE)) == 0)
            holder.check.visibility = View.INVISIBLE
    }

    /**
     * Método que devuelve el número de tareas
     * @return Número de tareas actual
     */
    override fun getItemCount(): Int {
        return this.tasks!!.size
    }

    inner class ViewHolderTask(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var taskId: String? = null
        var descriptionTask: TextView = itemView.descripcion_task
        var dateTask: TextView = itemView.fecha_task
        var menu: ImageView = itemView.menu_task
        var check: ImageView = itemView.DoneImageView
    }
}
