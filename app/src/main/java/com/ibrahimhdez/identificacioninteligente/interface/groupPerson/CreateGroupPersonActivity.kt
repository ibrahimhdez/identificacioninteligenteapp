package com.ibrahimhdez.identificacioninteligente.`interface`.groupPerson

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils.isEmpty
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.utils.Data
import com.ibrahimhdez.identificacioninteligente.faceAPI.Excepciones
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog

/**
 * Actividad para crear GroupPersons
 * @author Ibrahim Hernández Jorge
 */
class CreateGroupPersonActivity : AppCompatActivity(), AsyncResponse {
    lateinit var data: Data
    lateinit var groupIdEditText: EditText
    lateinit var groupNameEditText: EditText
    private lateinit var groupDesciptionEditText: EditText
    private var progress: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_groupperson_activity)

        iniciarComponentes()
    }

    /**
     * Mñetodo que inicia todos los componentes de la interfaz
     */
    private fun iniciarComponentes() {
        this.groupIdEditText = findViewById(R.id.groupIdEditText)
        this.groupNameEditText = findViewById(R.id.groupNameEditText)
        this.groupDesciptionEditText = findViewById(R.id.groupDescriptionEditText)
        this.progress = SpotsDialog(this,"Creating Group...", R.style.spotDialogStyle)
        this.progress!!.setCancelable(true)
        this.progress!!.setCanceledOnTouchOutside(false)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_add -> {
                if(camposVacios())
                    Toast.makeText(baseContext, "Fill required fields!", Toast.LENGTH_LONG).show()
                else {
                    obtenerValores()
                    execute()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método que comprueba si los campos obligatorios están vacíos
     * @return true si hay algún campo obligatorio, false si no lo hay
     */
    private fun camposVacios(): Boolean {
        return (isEmpty(groupIdEditText.text)) || (isEmpty(groupNameEditText.text))
    }

    /**
     * Método que obtiene los valores para crear el groupPerson
     */
    private fun obtenerValores() {
        this.data = Data(GroupPerson(this.groupIdEditText.text.toString().replace(" ", "").toLowerCase(), this.groupNameEditText.text.toString()))
    }

    /**
     * Métodoq que ejecuta la petición de crear un GroupPerson
     */
    private fun execute() {
        this.progress!!.show()
        Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_CREATE_PERSONGROUP, Comunicador.data!!, this)
    }

    /**
     * Método que será llamada cuando las tareas asíncronas hayan acabado
     * @param response Número de respuesta que devolerá la petición HTTP
     */
    override fun processFinish(response: Int) {
        this.progress!!.dismiss()
        if(response != Comunicador.successfulCall)
            Toast.makeText(baseContext, Excepciones.createException(response, true), Toast.LENGTH_LONG).show()
        else {
            Toast.makeText(baseContext, "Group created!", Toast.LENGTH_LONG).show()
            DBComunicator.getInstance().insertGroupPerson(this.data.groupPerson!!)
            finish()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }
}