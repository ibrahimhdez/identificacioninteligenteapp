package com.ibrahimhdez.identificacioninteligente.`interface`.groupPerson

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.getbase.floatingactionbutton.FloatingActionButton
import com.ibrahimhdez.identificacioninteligente.faceAPI.*
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas.ApiConsults
import com.ibrahimhdez.identificacioninteligente.`interface`.adapters.GroupPersonAdapter
import com.ibrahimhdez.identificacioninteligente.`interface`.person.ActionPersonActivity
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.utils.Data
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog

/**
 * Clase para el manejo de acciones de los GroupPersons (entrenar, borrar,...)
 * @author Ibrahim Hernández Jorge
 */
class ActionsGroupPersonActivity : AppCompatActivity(), GroupPersonDelegate, AsyncResponse {
    private var groups: ArrayList<GroupPerson>? = null
    private var recyclerGroups: RecyclerView? = null
    private var fabMenu: FloatingActionsMenu? = null
    private var fabCreate: FloatingActionButton? = null
    private var fabSync: FloatingActionButton? = null
    private var progress: SpotsDialog? = null
    private var adapter: GroupPersonAdapter? = null
    private var data: Data? = null
    private var scaledUp: Animation? = null
    private var scaledDown: Animation? = null
    private var noGroupsText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actions_groupperson_activity)

        iniciarComponentes()
    }

    override fun onRestart() {
        super.onRestart()
        actualizarRecycler()
    }

    /**
     * Método que inicia los componentes del Activity
     */
    private fun iniciarComponentes() {
        this.scaledUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
        this.scaledDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
        this.recyclerGroups = findViewById(R.id.recyclerView)
        this.noGroupsText = findViewById(R.id.noGroupsText)
        this.recyclerGroups!!.layoutManager = LinearLayoutManager(this)
        iniciarFloatingButton()
        actualizarRecycler()
    }

    private fun actualizarRecycler() {
        this.groups = obtainGroups()

        //Si no hay grupos, mostramos el mensaje de crear o sincronizar la aplicación con la nube
        if(this.groups!!.size == 0)
            this.noGroupsText!!.visibility = View.VISIBLE
        else
            this.noGroupsText!!.visibility = View.INVISIBLE

        //Obtener el adapter de los GroupPersons para añadírselo al RecyclerView de los grupos
        this.adapter = GroupPersonAdapter(this.groups!!)
        this.adapter!!.delegate = this
        this.recyclerGroups!!.adapter = adapter
    }

    private fun iniciarFloatingButton() {
        this.fabMenu = findViewById(R.id.menu_fab)
        this.fabCreate = findViewById(R.id.accion_add)
        this.fabCreate!!.setOnClickListener({ _ ->
            this.fabMenu!!.collapse()
            val intent = Intent(applicationContext, CreateGroupPersonActivity::class.java)
            startActivity(intent)
        })

        this.fabSync = findViewById(R.id.accion_sync)
        this.fabSync!!.setOnClickListener({ _ ->
            this.fabMenu!!.collapse()
            this.progress = iniciarSpotsDialog("Synchronizing...")
            this.progress!!.show()
            Comunicador.controlador!!.execute(ApiConsults.CONSULT_GET_LIST_PERSONGROUPS, Comunicador.data!!, this)
        })

        this.recyclerGroups!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && fabMenu!!.visibility == View.VISIBLE) {
                    fabMenu!!.startAnimation(scaledDown)
                    fabMenu!!.visibility = View.INVISIBLE
                }
                else if (dy < 0 && fabMenu!!.visibility != View.VISIBLE) {
                    fabMenu!!.startAnimation(scaledUp)
                    fabMenu!!.visibility = View.VISIBLE
                }
            }
        })
    }

    /**
     * Método que obtiene de la Base de Datos los GroupPersons vigentes
     * @return ArrayList con todos los GroupPersons alojados en la Base de Datos local
     */
    private fun obtainGroups(): ArrayList<GroupPerson>? {
        val arrayGroups = ArrayList<GroupPerson>()
        //Cursor que contiene los resultados de la llamada a la tabla "GROUPS"
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_GROUPS, null, null, 30)

        //Bucle que va sacando del cursor todos los objetos GroupPerson
        while (cursor.moveToNext())
            arrayGroups.add(GroupPerson(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NAME))))

        cursor.close()

        return arrayGroups
    }

    override fun processFinish(menu: ImageView, holder: GroupPersonAdapter.ViewHolderGroupPerson) {
        menu.setOnClickListener({
            val popupMenu = PopupMenu(menu.context, menu)
            popupMenu.menuInflater.inflate(R.menu.groupperson_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener({ item: MenuItem? ->
                this.data = Data(GroupPerson(holder.idGroupPerson.text as String, holder.nameGroupPerson.text as String))
                Comunicador.data!!.groupPerson = this.data!!.groupPerson

                when (item!!.itemId) {
                    R.id.view_persons -> {
                        val intent = Intent(applicationContext, ActionPersonActivity::class.java)
                        startActivity(intent)
                    }

                    R.id.train_option -> {
                        this.progress = iniciarSpotsDialog("Training...")
                        this.progress!!.show()
                        Comunicador.controlador!!.execute(ApiConsults.CONSULT_TRAIN, Comunicador.data!!, this)
                    }

                    R.id.delete_option -> {
                        Comunicador.controlador!!.execute(ApiConsults.CONSULT_DELETE_PERSONGROUP, Comunicador.data!!, this)
                        DBComunicator.getInstance().deleteGroupPerson(this.data!!.groupPerson!!)
                        if(this.fabMenu!!.visibility == View.INVISIBLE) {
                            fabMenu!!.startAnimation(scaledUp)
                            fabMenu!!.visibility = View.VISIBLE
                        }
                        this.progress = iniciarSpotsDialog("Deleting group...")
                        this.progress!!.show()
                    }
                }
                true
            })
            popupMenu.show()
        })
    }

    override fun processFinish(response: Int) {
        this.progress!!.dismiss()
        if(response != Comunicador.successfulCall) {
            if (response == 202)
                Toast.makeText(baseContext, "Group trained!", Toast.LENGTH_LONG).show()
            else
                Toast.makeText(baseContext, Excepciones.createException(response, true), Toast.LENGTH_LONG).show()
        }
        else {
            Toast.makeText(baseContext, "Group deleted!", Toast.LENGTH_LONG).show()
            actualizarRecycler()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }

    override fun processFinish(consulta: ApiConsults) {
        Comunicador.controlador!!.cloudSynchronization(this.progress!!, this)
        actualizarRecycler()
        Toast.makeText(baseContext, "Synchronized!", Toast.LENGTH_LONG).show()
        println(DBComunicator.getInstance().getTableAsString("USERS"))
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //No permitir volver hacia atrás desde esta activity
    override fun onBackPressed() {

    }

    private fun iniciarSpotsDialog(message: String): SpotsDialog {
        val spots = SpotsDialog(this, message, R.style.spotDialogStyle)
        spots.setCancelable(true)
        spots.setCanceledOnTouchOutside(false)

        return spots
    }
}