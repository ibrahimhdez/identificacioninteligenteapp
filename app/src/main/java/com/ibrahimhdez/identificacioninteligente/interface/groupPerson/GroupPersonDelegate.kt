package com.ibrahimhdez.identificacioninteligente.`interface`.groupPerson

import android.widget.ImageView
import com.ibrahimhdez.identificacioninteligente.`interface`.adapters.GroupPersonAdapter

interface GroupPersonDelegate {
    fun processFinish(menu: ImageView, holder: GroupPersonAdapter.ViewHolderGroupPerson)
}