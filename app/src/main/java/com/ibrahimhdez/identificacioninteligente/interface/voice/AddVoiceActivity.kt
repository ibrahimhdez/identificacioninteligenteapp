package com.ibrahimhdez.identificacioninteligente.`interface`.voice

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.MediaRecorder
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.IdentifyActivity
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.SpeakerConsults
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.WavAudioRecorder

/**
 * Activity para el añadido de voz a personas
 * @author Ibrahim Hernández Jorge
 */
class AddVoiceActivity: AppCompatActivity(), AsyncResponse {
    private var voiceButton: Button? = null
    private var mediaRecorder: WavAudioRecorder? = null
    private var recording = false
    private var done = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_voice_activity)

        grantPermissions()
        iniciarComponentes()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método para obtener los permisos de grabar audio y de guardar en almacenamiento externo
     */
    private fun grantPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), IdentifyActivity.RECORD_AUDIO)
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), IdentifyActivity.EXTERNAL_STORAGE)
    }

    private fun iniciarComponentes() {
        this.voiceButton = findViewById(R.id.addVoiceButton)
        this.voiceButton!!.setOnClickListener({
            if (this.recording) {
                val stringNoRecording = "Press for Record Voice"

                this.recording = false
                this.mediaRecorder!!.stop()
                this.mediaRecorder!!.release()
                this.voiceButton!!.text = stringNoRecording
                this.done = true
            } else {
                val stringRecording = "Recording... Press to finish"
                val destination = "/sdcard/userVoice.wav"
                this.mediaRecorder = WavAudioRecorder(MediaRecorder.AudioSource.MIC, 16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT)

                this.mediaRecorder!!.setOutputFile(destination)
                this.mediaRecorder!!.prepare()
                this.mediaRecorder!!.start()
                this.voiceButton!!.text = stringRecording
                this.recording = true
            }
        })

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_add -> {
                if(!this.done)
                    Toast.makeText(this, "Please, first record your voice!", Toast.LENGTH_LONG).show()
                else {
                    this.done = false
                    val cursor = DBComunicator.getInstance().rawConsult("SELECT ${DBContract.DBEntry.VOICE_ID} FROM ${DBContract.DBEntry.TABLA_PERSONS} " +
                            "WHERE ${DBContract.DBEntry.ID} = '${Comunicador.data!!.person!!.personId}'")
                    if(cursor.moveToFirst()) {
                        Comunicador.data!!.profileId = cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.VOICE_ID))
                        if(Comunicador.data!!.profileId != "")
                            Comunicador.voiceController!!.execute(SpeakerConsults.ApiConsults.CALL_CREATE_ENROLLMENT, Comunicador.data!!, this)
                        else
                            Toast.makeText(this, "This person has no profile for voice, create one!", Toast.LENGTH_LONG).show()
                    }
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun processFinish(response: Int) {
        if(response != 202)
            Toast.makeText(this, "Error, please, try again!", Toast.LENGTH_LONG).show()
        else {
            Toast.makeText(this, "Voice added successfully!", Toast.LENGTH_LONG).show()
            finish()
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {

    }


}