package com.ibrahimhdez.identificacioninteligente.`interface`.adapters

import android.content.Intent
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.IdentifyActivity
import com.ibrahimhdez.identificacioninteligente.`interface`.groupPerson.GroupPersonDelegate
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.preferences.Preferencias
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import java.util.ArrayList

/**
 * Adaptador de GroupPerson para que sea mostrado en un RecycleView
 * @author Ibrahim Hernández Jorge
 */
class GroupPersonAdapter(groups: ArrayList<GroupPerson>) : RecyclerView.Adapter<GroupPersonAdapter.ViewHolderGroupPerson>() {
    private var groups: ArrayList<GroupPerson>? = groups
    var delegate: GroupPersonDelegate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderGroupPerson {
         AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
         val view = LayoutInflater.from(parent.context).inflate(R.layout.card_groupperson, parent, false)

         return ViewHolderGroupPerson(view)
    }

    override fun onBindViewHolder(holder: ViewHolderGroupPerson, position: Int) {
        holder.idGroupPerson.text = this.groups!![position].id
        holder.nameGroupPerson.text = this.groups!![position].name

        delegate!!.processFinish(holder.menu, holder)
    }

    override fun getItemCount(): Int {
        return this.groups!!.size
    }

    inner class ViewHolderGroupPerson(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var idGroupPerson: TextView = itemView.findViewById(R.id.id_groupperson)
        var nameGroupPerson: TextView = itemView.findViewById(R.id.name_groupperson)
        var menu: ImageView = itemView.findViewById(R.id.menu_imageview)

        init {
            itemView.setOnClickListener({ view ->
               Comunicador.data!!.groupPerson = GroupPerson(idGroupPerson.text as String, nameGroupPerson.text as String)
               Preferencias.putStringPreferencias(itemView.context, Preferencias.group, Comunicador.data!!.groupPerson!!.id)
               val intent = Intent(view.context, IdentifyActivity::class.java)
               view.context.startActivity(intent)
           })
        }
    }
}
