package com.ibrahimhdez.identificacioninteligente.`interface`.person

import android.widget.ImageView
import com.ibrahimhdez.identificacioninteligente.`interface`.adapters.PersonAdapter

interface PersonDelegate {
    fun processFinish(menu: ImageView, holder: PersonAdapter.ViewHolderPerson)
}