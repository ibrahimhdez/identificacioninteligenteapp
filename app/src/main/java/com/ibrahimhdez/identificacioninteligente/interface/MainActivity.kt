package com.ibrahimhdez.identificacioninteligente.`interface`

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.`interface`.adapters.TaskAdapter
import com.ibrahimhdez.identificacioninteligente.IdentifyActivity
import com.ibrahimhdez.identificacioninteligente.`interface`.tasks.AddTaskActivity
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Tasks
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AsyncResponse {
    private var userName: TextView? = null
    private var recyclerTasks: RecyclerView? = null
    private var adapter: TaskAdapter? = null
    private var tasks: ArrayList<Tasks>? = null
    private var scaledUp: Animation? = null
    private var scaledDown: Animation? = null
    private var noTasksTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        title = Comunicador.data!!.person!!.name

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        iniciarComponentes()
    }

    /**
     * Método para iniciar los componentes del Activity
     */
    private fun iniciarComponentes() {
        this.scaledUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
        this.scaledDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
        this.recyclerTasks = findViewById(R.id.recyclerTasks)
        this.noTasksTextView = mainTextView
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        this.userName = navigationView.getHeaderView(0).findViewById(R.id.userNameTextView)
        val noTaskText = "Hello ${Comunicador.data!!.person!!.name}, you don´t have tasks yet, please, create one!"
        this.noTasksTextView!!.text = noTaskText
        this.userName!!.text = Comunicador.data!!.person!!.name
        this.recyclerTasks!!.layoutManager = LinearLayoutManager(this)
        iniciarFloatingActionMenu()
        actualizarRecycler()
    }

    /**
     * Método que se ejecutará cando se vuelva a esta Activity
     */
    override fun onRestart() {
        super.onRestart()
        actualizarRecycler()
    }

    /**
     * Método que se ejecutará cuando se pulse en vovler hacia atrás
     */
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    /**
     * Método para implementar las acciones de los Floating Action Buttons del Activity
     */
    private fun iniciarFloatingActionMenu() {
        accion_add_task.setOnClickListener({
            menu_fab.collapse()
            startActivity(Intent(this, AddTaskActivity::class.java))
        })

        this.recyclerTasks!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && menu_fab.visibility == View.VISIBLE) {
                    menu_fab.startAnimation(scaledDown)
                    menu_fab.visibility = View.INVISIBLE
                }
                else if (dy < 0 &&  menu_fab.visibility != View.VISIBLE) {
                    menu_fab.startAnimation(scaledUp)
                    menu_fab.visibility = View.VISIBLE
                }
            }
        })
    }

    /**
     * Método que actualizará el Recycler de Tareas
     */
    private fun actualizarRecycler() {
        this.tasks = obtainTasks()

        //Si no hay tareas, mostramos el mensaje de crear o sincronizar la aplicación con la nube
        if(this.tasks!!.size == 0)
            this.noTasksTextView!!.visibility = View.VISIBLE
        else
            this.noTasksTextView!!.visibility = View.INVISIBLE

        //Obtener el adapter de los GroupPersons para añadírselo al RecyclerView de los grupos
        this.adapter = TaskAdapter(this.tasks!!)
        this.adapter!!.delegate = this
        //Igualamos el método de refrescar un recycler al de esta clase
        this.adapter!!.refreshRecycler = { actualizarRecycler() }
        //this.adapter!!.delegate = this
        this.recyclerTasks!!.adapter = adapter
    }

    /**
     * Método que obtiene de la base de datos todas las tareas del usuario
     * @return ArrayList con todas las tareas del usuario
     */
    private fun obtainTasks(): ArrayList<Tasks> {
        val tasks = ArrayList<Tasks>()
        //Cursor que contiene los resultados de la llamada a la tabla "GROUPS"
        val cursor = DBComunicator.getInstance().rawConsult("SELECT * FROM ${DBContract.DBEntry.TABLA_TASKS} WHERE ${DBContract.DBEntry.PERSON_ID} = '${Comunicador.data!!.person!!.personId!!}'")

        //Bucle que va sacando del cursor todos los objetos GroupPerson
        while (cursor.moveToNext())
            tasks.add(Tasks(cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.DESCRIPCION)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.FECHA_LIMITE))))
        cursor.close()

        return tasks
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_calendar -> {
                startActivity(Intent(this, CalendarActivity::class.java))
            }

            R.id.nav_log_out -> {
                val intent = Intent(this, IdentifyActivity::class.java)
                intent.putExtra("LOGOUT", true)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun processFinish(response: Int) {
        actualizarRecycler()
    }

    override fun processFailed() {

    }
}
