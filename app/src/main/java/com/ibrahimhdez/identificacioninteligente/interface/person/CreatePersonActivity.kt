package com.ibrahimhdez.identificacioninteligente.`interface`.person

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.ibrahimhdez.identificacioninteligente.faceAPI.Excepciones
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.R
import com.ibrahimhdez.identificacioninteligente.database.DBComunicator
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.Consultas
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.SpeakerConsults
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import dmax.dialog.SpotsDialog

/**
 * Actividad para el manejo de creación de personas
 */
class CreatePersonActivity: AppCompatActivity(), AsyncResponse {
    private lateinit var personNameEditText: EditText
    private lateinit var personDesciptionEditText: EditText
    var passwordEditText: EditText? = null
    private var progress: AlertDialog? = null
    private var creatingPerson = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_person_activity)

        iniciarComponentes()
    }

    private fun iniciarComponentes() {
        this.personNameEditText = findViewById(R.id.personNameEditText)
        this.personDesciptionEditText = findViewById(R.id.personDescriptionEditText)
        this.passwordEditText = findViewById(R.id.passEditText)
        this.progress = SpotsDialog(this, "Creating Person", R.style.spotDialogStyle)
        this.progress!!.setCancelable(true)
        this.progress!!.setCanceledOnTouchOutside(false)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_add -> {
                if(camposVacios())
                    Toast.makeText(baseContext, "Fill required fields!", Toast.LENGTH_LONG).show()
                else {
                    this.creatingPerson = true
                    obtenerValores()
                    execute()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun camposVacios(): Boolean {
        return (TextUtils.isEmpty(personNameEditText.text) || (TextUtils.isEmpty(passwordEditText!!.text)))
    }

    private fun obtenerValores() {
        val person = Person(this.personNameEditText.text.toString())
        person.description = this.personDesciptionEditText.text.toString()

        Comunicador.data!!.person = person
    }

    /**
     * Métodoq que ejecuta la petición de crear una Person
     */
    private fun execute() {
        this.progress!!.show()
        Comunicador.controlador!!.execute(Consultas.ApiConsults.CONSULT_CREATE_PERSON, Comunicador.data!!, this)
    }

    override fun processFinish(response: Int) {
        if(response != Comunicador.successfulCall && this.creatingPerson) {
            this.progress!!.dismiss()
            Toast.makeText(baseContext, Excepciones.createException(response, false), Toast.LENGTH_LONG).show()
        }
        else {
            if(this.creatingPerson) {
                this.creatingPerson = false
                Comunicador.voiceController!!.execute(SpeakerConsults.ApiConsults.CALL_CREATE_PROFILE, Comunicador.data!!, this)
            }
            else {
                this.progress!!.dismiss()
                var voiceId = ""
                //Primero añadimos a la persona creada su clave de voz
                if(Comunicador.voiceController!!.json!!.has("identificationProfileId")) {
                    voiceId = Comunicador.voiceController!!.json!!.getString("identificationProfileId")
                    Comunicador.voiceController!!.cleanJSON()
                }

                if (Comunicador.controlador!!.json!!.has("personId")) {
                    Toast.makeText(baseContext, "Person created!", Toast.LENGTH_LONG).show()
                    Comunicador.data!!.person!!.personId = Comunicador.controlador!!.json!!.getString("personId")
                    Comunicador.controlador!!.cleanJSON(false)
                    DBComunicator.getInstance().insertPerson(Comunicador.data!!.person!!, Comunicador.data!!.groupPerson!!, voiceId, this.passwordEditText!!.text.toString())
                    Comunicador.serverManager!!.insertUser(Comunicador.data!!.person!!.name, Comunicador.data!!.groupPerson!!.id, this.passwordEditText!!.text.toString())
                    println(DBComunicator.getInstance().getTableAsString(DBContract.DBEntry.TABLA_PERSONS))
                    finish()
                } else
                    Toast.makeText(this, "An error occurred, please try again.", Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Método que se ejecutará si hubo un error de conexión mientras se hacía la consulta HTTP
     */
    override fun processFailed() {
        if(this.progress != null) {
            this.progress!!.dismiss()
            Toast.makeText(this, "No Internet connection, please try again later!", Toast.LENGTH_LONG).show()
        }
    }
}