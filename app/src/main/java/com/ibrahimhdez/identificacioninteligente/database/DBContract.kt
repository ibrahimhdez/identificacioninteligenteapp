package com.ibrahimhdez.identificacioninteligente.database

object DBContract {
    class DBEntry {
        companion object {
            const val ID = "ID"
            const val NAME = "NAME"

            //Tabla Grupos
            const val TABLA_GROUPS = "GROUPS"

            //Tabla Personas
            const val TABLA_PERSONS = "PERSONS"
            const val GROUP_ID = "GROUP_ID"
            const val VOICE_ID = "VOICE_ID"

            //Tabla Caras
            const val TABLA_FACES = "FACES"
            const val PERSON_ID = "PERSON_ID"

            //Tabla Users
            const val TABLA_USERS = "USERS"
            const val USER = "USER"
            const val PASSWORD = "PASSWORD"

            //Tabla TASKS
            const val TABLA_TASKS = "TASKS"
            const val DESCRIPCION = "DESCRIPCION"
            const val FECHA_LIMITE = "FECHA_LIMITE"
            const val DONE = "DONE"
        }
    }
}