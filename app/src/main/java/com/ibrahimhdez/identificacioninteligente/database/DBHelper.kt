package com.ibrahimhdez.identificacioninteligente.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import android.content.ContentValues
import android.database.Cursor
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Tasks

/**
 * Clase que gestiona la base de datos de la aplicación
 * @author Ibrahim Hernández Jorge
 */
class DBHelper internal constructor(context : Context) : SQLiteOpenHelper(context, "database", null, 1){
    /**
     * Tablas de la base de datos
     */
    companion object {
        //Constante que cuando se ejecuta en SQL crea la tabla GROUPS
        const val SQL_CREATE_GRUPOS =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_GROUPS + "(" +
                        DBContract.DBEntry.ID + " TEXT(20) PRIMARY KEY, " +
                        DBContract.DBEntry.NAME + " TEXT(20) NOT NULL " +
                        "); "

        //Constante que cuando se ejecuta en SQL crea la tabla USERS
        const val SQL_CREATE_USERS =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_USERS + "(" +
                        DBContract.DBEntry.USER + " TEXT(20), " +
                        DBContract.DBEntry.PERSON_ID + " TEXT(20) REFERENCES " +
                        DBContract.DBEntry.TABLA_PERSONS + "(" +
                        DBContract.DBEntry.ID + ") "+
                        "ON DELETE CASCADE, " +
                        DBContract.DBEntry.PASSWORD + " TEXT(20), " +
                        "PRIMARY KEY( " + DBContract.DBEntry.USER + //", " + DBContract.DBEntry.GROUP_ID +
                        "));"

        //Constante que cuando se ejecuta en SQL crea la tabla PERSONS
        const val SQL_CREATE_PERSONS =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_PERSONS + "(" +
                        DBContract.DBEntry.ID + " TEXT(100) PRIMARY KEY, " +
                        DBContract.DBEntry.NAME + " TEXT(50), " +
                        DBContract.DBEntry.VOICE_ID + " TEXT(50), " +
                        DBContract.DBEntry.GROUP_ID + " TEXT(20) REFERENCES " +
                        DBContract.DBEntry.TABLA_GROUPS + "(" +
                        DBContract.DBEntry.ID + ") "+
                        "ON DELETE CASCADE, " +
                        DBContract.DBEntry.USER + " TEXT(20) REFERENCES " +
                        DBContract.DBEntry.TABLA_USERS + "(" +
                        DBContract.DBEntry.USER + ") " +
                        "ON DELETE CASCADE" +
                        "); "

        //Constante que cuando se ejecuta en SQL crea la tabla FACES
        const val SQL_CREATE_FACES =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_FACES + "(" +
                        DBContract.DBEntry.ID + " TEXT(100), " +
                        DBContract.DBEntry.NAME + " TEXT(100), " +
                        DBContract.DBEntry.PERSON_ID + " TEXT(100), " +
                        "FOREIGN KEY " + "(${DBContract.DBEntry.PERSON_ID})" + " REFERENCES " +
                        DBContract.DBEntry.TABLA_PERSONS + "(" +
                        DBContract.DBEntry.ID + ") " +
                        "ON DELETE CASCADE, " +
                        "PRIMARY KEY( " + DBContract.DBEntry.ID + //", " + DBContract.DBEntry.NAME +
                        "));"

        //Constante que crea la tabla TASKS
        const val SQL_CREATE_TASKS =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_TASKS + "(" +
                        DBContract.DBEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DBContract.DBEntry.DESCRIPCION + " TEXT(70), " +
                        DBContract.DBEntry.FECHA_LIMITE + " TEXT(10), " +
                        DBContract.DBEntry.DONE + " INTEGER DEFAULT 0, " +
                        DBContract.DBEntry.PERSON_ID + " TEXT(100), " +
                        "FOREIGN KEY " + "(${DBContract.DBEntry.PERSON_ID})" + " REFERENCES " +
                        DBContract.DBEntry.TABLA_PERSONS + "(" +
                        DBContract.DBEntry.ID + ") " +
                        "ON DELETE CASCADE);"
    }

    /**
     * Método que se inicia en primer momento. Crea todas las tablas
     * @param db Objeto SQLiteDatabase
     */
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_GRUPOS)
        db?.execSQL(SQL_CREATE_PERSONS)
        db?.execSQL(SQL_CREATE_FACES)
        db?.execSQL(SQL_CREATE_USERS)
        db?.execSQL(SQL_CREATE_TASKS)
    }

    /**
     * Método que se ejecuta cuando se actualiza la base de datos
     * @param db Objeto SQLiteDatabase
     * @param p1 versión anterior
     * @param p2 nueva versión
     */
    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        destroy(db)
        onCreate(db)
    }

    /**
     * Método que destruye las tablas de la base de datos
     * @param db Writable de la base de datos
     */
    private fun destroy(db: SQLiteDatabase?) {
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_GROUPS}")
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_PERSONS}")
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_FACES}")
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_USERS}")
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_TASKS}")
    }

    /**
     * Método para insertar un GroupPerson en la base de datos
     * @groupPerson Grupo que será insertado
     */
    fun insertGroupPerson(groupPerson: GroupPerson) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction() //Comienza la transacción

        //Inserta los datos del grupo en un registro
        registro.put(DBContract.DBEntry.ID, groupPerson.id)
        registro.put(DBContract.DBEntry.NAME, groupPerson.name)

        bd.insert(DBContract.DBEntry.TABLA_GROUPS, null, registro) //Inserta el registro en la base de datos
        closeTransaction(bd)
    }

    /**
     * Método para insertar un usuario en la base de datos
     * @param user Nombre del usuario, coincidirá con el nombre de la persona
     * @param password Contraseña del usuario
     */
    private fun insertUser(user: String, personId: String, password: String, bd: SQLiteDatabase) {
        val registro = ContentValues()

        bd.beginTransaction() //Comienza la transacción

        //Inserta los datos de un usuario en un registro
        registro.put(DBContract.DBEntry.USER, user)
        registro.put(DBContract.DBEntry.PERSON_ID, personId)
        registro.put(DBContract.DBEntry.PASSWORD, password)

        //Inserta el registro en la base de datos
        bd.insert(DBContract.DBEntry.TABLA_USERS, null, registro)
        //No confirmamos transacción porque de eso ya se encarga insertPerson, método desde donde se llama automáticamente a este
    }

    /**
     * Método para insertar personas en la base de datos
     * @param person Objeto person que contendrá la información de la persona a insertar
     * @param groupPerson Objeto groupperson que contendrá la información del grupo donde estará la persona
     * @param password Contraseña para login alternativo
     */
    fun insertPerson(person: Person, groupPerson: GroupPerson, voiceId: String, password: String) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        insertUser(person.name, person.personId!!, password, bd)

        //Inserta los datos de la persona en un registro
        registro.put(DBContract.DBEntry.ID, person.personId)
        registro.put(DBContract.DBEntry.NAME, person.name)
        registro.put(DBContract.DBEntry.VOICE_ID, voiceId)
        registro.put(DBContract.DBEntry.GROUP_ID, groupPerson.id)
        registro.put(DBContract.DBEntry.USER, person.name)

        //Inserta el registro en la base de datos
        bd.insert(DBContract.DBEntry.TABLA_PERSONS, null, registro)
        //Confirmamos y cerramos la transacción
        closeTransaction(bd)
    }

    /**
     * Método para insertar caras en la tabla FACES
     * @param faceName Nombre de la cara
     * @param person Objeto Person que será la propietaria de la cara que estamos insertando
     */
    fun insertFace(faceName: String, person: Person) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction() //Comienza la transacción

        //Inserta los datos de la cara en un registro
        registro.put(DBContract.DBEntry.ID, person.persistedFaceId)
        registro.put(DBContract.DBEntry.NAME, faceName)
        registro.put(DBContract.DBEntry.PERSON_ID, person.personId)

        //Inserta el registro en la base de datos
        bd.insert(DBContract.DBEntry.TABLA_FACES, null, registro)
        //Confirmamos y cerramos la transacción
        closeTransaction(bd)
    }

    /**
     * Método para insertar tareas en la tabla TASKS
     * @param Descripción de la tarea
     * @param date Fecha máxima de finalización de la tarea
     * @param person Objeto Person que será la propietaria de la tarea
     */
    fun insertTask(descripcion: String, date: String, person: Person) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction() //Comienza la transacción

        //Insertar los datos de la tarea en el registro
        registro.put(DBContract.DBEntry.DESCRIPCION, descripcion)
        registro.put(DBContract.DBEntry.FECHA_LIMITE, date)
        registro.put(DBContract.DBEntry.PERSON_ID, person.personId)

        //Insertar registro en base de datos
        bd.insert(DBContract.DBEntry.TABLA_TASKS, null, registro)
        //Confirmar y cerrar la transacción
        closeTransaction(bd)
    }

    fun deleteGroupPerson(groupPerson: GroupPerson) {
        val bd = this.writableDatabase

        bd.execSQL("PRAGMA foreign_keys=ON;")
        bd.beginTransaction() //Comienza la transacción
        bd.delete(DBContract.DBEntry.TABLA_GROUPS, "${DBContract.DBEntry.ID}='${groupPerson.id}'", null)
        closeTransaction(bd)
    }

    fun deletePerson(person: Person) {
        val bd = this.writableDatabase

        bd.execSQL("PRAGMA foreign_keys=ON;")
        bd.beginTransaction()
        bd.delete(DBContract.DBEntry.TABLA_PERSONS, "${DBContract.DBEntry.ID}='${person.personId}'", null)
        closeTransaction(bd)
    }

    fun deleteTask(task: Tasks) {
        val bd = this.writableDatabase

        bd.beginTransaction()
        bd.delete(DBContract.DBEntry.TABLA_TASKS, "${DBContract.DBEntry.ID}='${task.id}'", null)
        closeTransaction(bd)
    }

    fun deleteFaceByName(name: String, person: Person) {
        val bd = this.writableDatabase

        bd.beginTransaction()
        bd.delete(DBContract.DBEntry.TABLA_FACES, "${DBContract.DBEntry.PERSON_ID}='${person.personId!!}' AND ${DBContract.DBEntry.NAME}='$name'", null)
        closeTransaction(bd)
    }

    /**
     * Método que cierra y confirma una transacción
     * @param bd Objeto base de datos con permisos de escritura en la que será confirmada la transacción
     */
    private fun closeTransaction(bd: SQLiteDatabase) {
        bd.setTransactionSuccessful() //Confirmar la transacción para que los cambios sean guardados
        bd.endTransaction() //Finalizar la transacción
        bd.close() //Cerrar base de datos
    }

    /**
     * Método para realizar una consulta personalizada
     * @return Cursor con el resultado de la consulta
     */
    private fun cursorConsultaPersonalizada(tabla: String, tableColumns: Array<String>?, whereClause: String?, whereArgs: Array<String>?,
                                    groupBy: String?, having: String?, orderBy: String?, limite: Int? = null): Cursor {
        return this.readableDatabase.query(tabla, tableColumns, whereClause, whereArgs, groupBy, having, orderBy, limite?.toString())
    }

    /**
     * Método para realizar consulta
     * @return Cursor con el resultado de la consulta
     */
    fun cursorConsulta(tabla: String, atributos: Array<String>?, valores: Array<String>?, limite: Int? = null): Cursor {
        return if (atributos != null) {
            var whereClause = atributos[0] + "= ?"
            for (i in 1 until atributos.size)
                whereClause += " AND " + atributos[i] + "= ?"
            cursorConsultaPersonalizada(tabla, null, whereClause, valores, null, null, null, limite)
        } else
            cursorConsultaPersonalizada(tabla, null, null, null, null, null, null, limite)
    }

    /**
     * Método para cambiar el valor de un atributo de una determinada tabla de la base de datos
     * @param tableName Nombre de la tabla donde se efectuará el cambio
     * @param attribute Nombre del atributo que cambiará de valor
     * @param value Nuevo valor que tomará el atributo
     * @param primaryKey Nombre de la clave primaria de la tabla
     * @param valuePrimaryKey Valor de la clave primaria de la tabla
     */
    fun updateValue(tableName: String, attribute: String, value: String, primaryKey: String, valuePrimaryKey: String) {
        val bd = this.writableDatabase

        bd.beginTransaction()

        bd.execSQL("UPDATE $tableName SET $attribute='$value' WHERE $primaryKey='$valuePrimaryKey' ")
        closeTransaction(bd)
    }

    /**
     * Método que devuelve una tabla formateada a String para mostrar
     * @return String con el resultado de mostrar la tabla
     */
    fun getTableAsString(tableName: String): String {
        val db = this.readableDatabase
        var tableString = String.format("Table %s:\n", tableName)
        val allRows = db.rawQuery("SELECT * FROM $tableName", null)
        if (allRows.moveToFirst()) {
            val columnNames = allRows.columnNames
            do {
                for (name in columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)))
                }
                tableString += "\n"

            } while (allRows.moveToNext())
        }
        allRows.close()
        return tableString
    }

    /**
     * Método para realizar una consulta raw
     * @param consult Consulta en String
     * @return Objeto cursor con el resultado de la consulta
     */
    fun rawConsult(consult: String): Cursor {
        return this.readableDatabase.rawQuery(consult, null)
    }
}