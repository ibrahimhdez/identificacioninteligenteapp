package com.ibrahimhdez.identificacioninteligente.utils

import android.graphics.Bitmap
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.GroupPerson
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Person
import com.ibrahimhdez.identificacioninteligente.faceAPI.components.Tasks

/**
 * Clase que gestiona los datos que serán incluidos en las consultas
 * @author Ibrahim Hernández Jorge
 */
class Data {
    var groupPerson: GroupPerson? = null
    var person: Person? = null
    var task: Tasks? = null
    var faceIds: String? = null
    var profileId: String? = null
    var profiles: ArrayList<String>? = null
    private var urlImagen: String? = null
    var resultPersonId: String? = null
    var bitMap: Bitmap? = null

    constructor()

    constructor(groupPerson: GroupPerson){
        this.groupPerson = groupPerson
    }

    constructor(groupPerson: GroupPerson, person: Person, faceIds: String, urlImagen: String){
        this.groupPerson = groupPerson
        this.person = person
        this.faceIds = faceIds
        this.urlImagen = urlImagen
    }
}