package com.ibrahimhdez.identificacioninteligente.utils

import android.content.Context
import com.ibrahimhdez.identificacioninteligente.faceAPI.Controlador
import com.ibrahimhdez.identificacioninteligente.serverBBDD.ServerManager
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.VoiceController

/**
 * Clase que actúa de comunicador para que todos los paquetes accedan a la misma instancia de Controlador y a otros valores
 * @author Ibrahim Hernández Jorge
 */
object Comunicador {
    var controlador: Controlador? = null
    var voiceController: VoiceController? = null
    var serverManager: ServerManager? = null
    var data: Data? = null
    var context: Context? = null
    const val successfulCall: Int = 200
    var defaultPassword = "0000"
}
