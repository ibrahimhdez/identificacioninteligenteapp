package com.ibrahimhdez.identificacioninteligente.preferences

import android.content.Context
import android.content.SharedPreferences

object Preferencias {
    val nomPreferencias = "Preferencias" /** Nombre de las preferencias que vamos a utilizar  */
    val group = "group"
    private val ERROR_STRING = "No existe el String"

    /**
     * Método que inserta Strings en las preferencias
     * @param context Contexto desde el que se accede a las preferencias
     * @param clave Identificador del String que vamos a introducir y que nos va a peritir obtenerlo
     * @param valor Cadena de caracteres que se desea insertar en las preferencias
     */
    fun putStringPreferencias(context: Context, clave: String, valor: String) {
        val editor = getPreferencias(context, nomPreferencias).edit()
        editor.putString(clave, valor)
        editor.commit()
    }

    /**
     * Método que obtiene un String de las preferencias
     * @param context Contexto desde el que se accede a las preferencias
     * @param clave identificador del String que queremos acceder
     * @return String obtenido de las preferencias
     */
    fun getStringPreferencias(context: Context, clave: String): String {
        return getPreferencias(context, nomPreferencias).getString(clave, ERROR_STRING + clave)
    }

    /**
     * Método que devuelve las preferencias
     * @param contexto Contexto desde el que se accede a las preferencias
     * @param nombrePreferencias Nombre de las preferencias a las que se quiere acceder
     * @return Las preferencias cuyo nombre le hemos pasado
     */
    private fun getPreferencias(contexto: Context, nombrePreferencias: String): SharedPreferences {
        return contexto.getSharedPreferences(nombrePreferencias, Context.MODE_PRIVATE)
    }

    /**
     * Método que vacía las preferencias
     * @param context
     */
    fun vaciarPreferencias(context: Context) {
        val editor = getPreferencias(context, nomPreferencias).edit()
        editor.clear()
        editor.commit()

    }
}
