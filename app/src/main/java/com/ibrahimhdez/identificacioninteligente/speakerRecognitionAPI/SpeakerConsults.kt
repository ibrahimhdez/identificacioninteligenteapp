package com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI

import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.asyncTasks.VoiceOutputStreamAsyncTask
import com.ibrahimhdez.identificacioninteligente.utils.Data
import java.net.HttpURLConnection
import java.net.URL

/**
 * Clase que gestiona las Consultas a la API de Reconocimiento de Voz
 * @author Ibrahim Hernández Jorge
 */
class SpeakerConsults {
    enum class JsonConsults constructor(var header: String, var body: String) {
        JSON_CREATE_PROFILE("Content-Type,application/json", "{\"locale\":\"es-Es\"}"),
        JSON_CREATE_ENROLLMENT("Content-Type,application/octet-stream", ""),
        JSON_IDENTIFY("Content-Type,application/octet-stream", ""),
        JSON_DELETE_PROFILE("Content-Type,application/json", "");

        val headerKey = "Ocp-Apim-Subscription-Key,3ca2df6568dd41029bfefcb307326b14"
    }

    enum class ApiConsults constructor(var requestMethod: String, private var modulo: String, private var profileId: String, var json: JsonConsults) {
        CALL_CREATE_PROFILE("POST", "identificationProfiles", "", JsonConsults.JSON_CREATE_PROFILE),
        CALL_CREATE_ENROLLMENT("POST", "identificationProfiles", "/GET_PROFILEID/enroll?shortAudio=true", JsonConsults.JSON_CREATE_ENROLLMENT),
        CALL_IDENTIFY("POST", "identify?", "identificationProfileIds=GET_GROUPPROFILES&shortAudio=true", JsonConsults.JSON_IDENTIFY),
        CALL_DELETE_PROFILE("DELETE", "identificationProfiles", "/GET_PROFILEID", JsonConsults.JSON_DELETE_PROFILE);

        private val urlServidor = "https://westus.api.cognitive.microsoft.com/spid/v1.0/" //Servidor al que se realizan las peticiones de la API
        var httpConnection: HttpURLConnection? = null //Cliente HTTP para realizar las consultas
        private var outputStreamAsyncTask: VoiceOutputStreamAsyncTask? = null
        var delegate: AsyncResponse? = null
        var consulta: String
        private var consultaCopy: String

        init {
            consulta = obtenerUrlConsulta()
            consultaCopy = consulta
        }

        /**
         * Método que obtiene la consulta asociada a cada petición
         * @return Devuelve la consulta construida
         */
        private fun obtenerUrlConsulta(): String {
            return urlServidor + modulo + profileId
        }

        /**
         * Método que inserta en la consulta los valores de los atributos necesarios
         * @param data Objeto que contedrá los datos de los atributos
         */
        fun replaceData(data: Data) {
            if(this.consulta != this.consultaCopy)
                this.consulta = this.consultaCopy

            if (consulta.contains("GET_PROFILEID"))
                consulta = consulta.replace("GET_PROFILEID", data.profileId!!)
            if (consulta.contains("GET_GROUPPROFILES")) {
                var profiles = ""

                for(i in 0 until data.profiles!!.size) {
                    profiles += data.profiles!![i]
                    if(i < (data.profiles!!.size - 1))
                        profiles += ","
                }
                consulta = consulta.replace("GET_GROUPPROFILES", profiles)
            }
           initHttpRequest()
        }

        /**
         * Función que inicia el cliente HTTP
         */
        private fun initHttpRequest(){
            this.httpConnection = URL(this.consulta).openConnection() as HttpURLConnection
            this.httpConnection!!.doInput = true

            if((this.requestMethod != "GET") && (this.requestMethod != "DELETE"))
                this.httpConnection!!.doOutput = true

            this.httpConnection!!.requestMethod = this.requestMethod

            setHeader()
            setBody()
        }

        /**
         * Método que crea el header del JSON
         */
        private fun setHeader() {
            val parametrosHeader = this.json.header.split(",")
            val parametrosHeaderKey = this.json.headerKey.split(",")

            if(parametrosHeader.size > 1)
                this.httpConnection!!.setRequestProperty(parametrosHeader[0], parametrosHeader[1])
            this.httpConnection!!.setRequestProperty(parametrosHeaderKey[0], parametrosHeaderKey[1])
        }

        /**
         * Método que crea el body del JSON
         */
        private fun setBody(){
            this.outputStreamAsyncTask = VoiceOutputStreamAsyncTask()
            this.outputStreamAsyncTask!!.delegate = this.delegate
            this.outputStreamAsyncTask!!.execute(this)
        }

    }

}