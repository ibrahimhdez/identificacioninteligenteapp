package com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI

import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.asyncTasks.VoiceConnectionAsyncTask
import com.ibrahimhdez.identificacioninteligente.utils.Data
import org.json.JSONObject

class VoiceController: AsyncResponse {
    private var consulta: SpeakerConsults.ApiConsults? = null
    var json: JSONObject? = null
    private var mainAsyncResponse: AsyncResponse? = null

    /**
     * Método que ejecuta una consulta
     * @param consulta Objeto con la consulta a ejecutar
     */
    fun execute(consulta: SpeakerConsults.ApiConsults, data: Data, asyncResponse: AsyncResponse) {
        this.mainAsyncResponse = asyncResponse
        //Guardamos en el atributo de la clase la consulta API para ejecutarla cuando este preparada
        this.consulta = consulta
        //Ponemos como delegado de la consulta esta clase, así cuando termine la tarea asíncrona se ejecutará el método finishProcess de esta clase
        consulta.delegate = this
        //Reemplazamos los datos dados por la interfaz en la consulta, cuando termine se ejecutará el finishProcess
        consulta.replaceData(data)
    }

    /**
     * Método que se ejecuta cuando esta clase es la delegada de una tarea asíncrona y esta termina de ejecutarse
     * @param response Código de respuesta que devuelve la consulta HTTP
     */
    override fun processFinish(response: Int) {
        val asyncTask = VoiceConnectionAsyncTask()
        //Asignamos la clase que invocó la acción como delegado de la clase que realiza las tareas asíncronas para cuando termine llame al método perteneciente
        asyncTask.delegate = this.mainAsyncResponse
        asyncTask.execute(this.consulta)
    }

    override fun processFailed() {
        this.mainAsyncResponse!!.processFailed()
    }

    /**
     * Método para limpiar el atributo JSON una vez sea usado
     */
    fun cleanJSON() {
        this.json = JSONObject("{}")
    }
}