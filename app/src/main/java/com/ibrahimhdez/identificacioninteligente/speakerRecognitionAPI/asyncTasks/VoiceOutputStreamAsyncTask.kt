package com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.asyncTasks

import android.os.AsyncTask
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.SpeakerConsults
import java.io.*

/**
 * Tarea asíncrona para insertar en una consulta el body de un JSON
 * @author Ibrahim Hernández Jorge
 */
internal class VoiceOutputStreamAsyncTask: AsyncTask<SpeakerConsults.ApiConsults, Void, Boolean>() {
    var delegate: AsyncResponse? = null

    companion object {
        const val BUFFER_SIZE = 4096
    }

    /**
     * Método que se ejecuta en background para añadir el JSON
     */
    override fun doInBackground(vararg consulta: SpeakerConsults.ApiConsults): Boolean {
        try {
            if ((consulta[0] == SpeakerConsults.ApiConsults.CALL_CREATE_ENROLLMENT) || (consulta[0] == SpeakerConsults.ApiConsults.CALL_IDENTIFY)) {
                val request = DataOutputStream(consulta[0].httpConnection!!.outputStream)
                val buffer = ByteArray(BUFFER_SIZE)
                var bytesRead = -1
                val inputStream = FileInputStream(File("/sdcard/userVoice.wav"))

                while ({bytesRead = inputStream.read(buffer); bytesRead}() != -1)
                    request.write(buffer, 0, bytesRead)

            } else if ((consulta[0].requestMethod != "GET") && (consulta[0].requestMethod != "DELETE")) {
                val bw = BufferedWriter(OutputStreamWriter(consulta[0].httpConnection!!.outputStream, "UTF-8"))
                bw.write(consulta[0].json.body)
                bw.flush()
                bw.close()
            }
        } catch (e: Exception) {
            return false
        }
        return true
    }

    override fun onPostExecute(result: Boolean?) {
        if(result!!)
            this.delegate!!.processFinish(0)
        else
            this.delegate!!.processFailed()
    }
}
