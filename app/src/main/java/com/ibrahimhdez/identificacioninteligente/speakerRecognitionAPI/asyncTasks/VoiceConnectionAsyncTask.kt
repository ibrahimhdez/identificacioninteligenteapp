package com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.asyncTasks

import android.os.AsyncTask
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.speakerRecognitionAPI.SpeakerConsults
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador
import org.json.JSONException
import org.json.JSONObject
import java.net.UnknownHostException
import java.io.*


/**
 * Clasa para realizar las peticiones Web en background para evitar la congelación del hilo principal de nuestra aplicación
 * @author Ibrahim Hernández Jorge
 */
internal class VoiceConnectionAsyncTask: AsyncTask<SpeakerConsults.ApiConsults, Void, SpeakerConsults.ApiConsults>() {
    var delegate: AsyncResponse? = null //Delegado que será la clase desde la que se llama a la tarea asíncrona
    private var failed: Boolean? = null

    /**
     * Método que hace la llamada y descarga en background la respuesta de la API
     * @param consulta Consulta a ejecutar
     * @return Número de respuesta HTTP
     */
    override fun doInBackground(vararg consulta: SpeakerConsults.ApiConsults): SpeakerConsults.ApiConsults {
        this.failed = false
        try {
            consulta[0].httpConnection!!.connect()
            println(consulta[0].consulta)
            println("HEADER !=!=!=!")
            println(consulta[0].httpConnection!!.headerFields)
            println(consulta[0].httpConnection!!.headerFields.values)
            //println(getStringFromInputStream(consulta[0].httpConnection!!.errorStream))
            val responseStream = BufferedInputStream(consulta[0].httpConnection!!.inputStream) //Hace la petición a la API
            val responseStreamReader = BufferedReader(InputStreamReader(responseStream)) //Recoge el resultado de la petición
            var line: String? = ""
            val stringBuilder = StringBuilder()

            //Bucle que recoge en una String la respuesta de la API
            while ({line = responseStreamReader.readLine(); line}() != null)
                stringBuilder.append(line).append("\n")

            responseStreamReader.close()
            Comunicador.voiceController!!.json = JSONObject(stringBuilder.toString())

        } catch (e: UnknownHostException) {
            this.failed = true
        } catch (e: JSONException) {

        } catch (e: FileNotFoundException) {

        }
        return consulta[0]
    }

    private fun getStringFromInputStream(`is`: InputStream): String {
        var br: BufferedReader? = null
        val sb = StringBuilder()
        var line: String?

        try {
            br = BufferedReader(InputStreamReader(`is`))
            line = br.readLine()
            while (line != null) {
                sb.append(line)
                line = br.readLine()
            }

        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (br != null) {
                try {
                    br.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        return sb.toString()
    }

    /**
     * Función que llama al método processFinish de la clase que llamó a esta tarea asíncrona
     * @param consulta
     */
    override fun onPostExecute(consulta: SpeakerConsults.ApiConsults) {
        if (this.delegate != null) {
            when {
                this.failed!! -> delegate!!.processFailed()
                else -> delegate!!.processFinish(consulta.httpConnection!!.responseCode)
            }
        }
    }
}


