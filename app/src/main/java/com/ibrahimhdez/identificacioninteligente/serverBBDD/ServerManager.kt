package com.ibrahimhdez.identificacioninteligente.serverBBDD

import android.database.Cursor
import com.ibrahimhdez.identificacioninteligente.database.DBContract
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL

class ServerManager: AsyncResponse {
    private var jsonRequest = JSONObject()
    var jsonResult = JSONObject()
    var insert = false
    var downloadAsyncTask: DownloadAsyncTask? = null

    companion object {
        const val URL_SERVER = "192.168.1.177:8080/"
    }

    private fun initRequest() {
        this.downloadAsyncTask = DownloadAsyncTask()
        this.jsonRequest = JSONObject()
    }

    fun getPasswords(cursor: Cursor) {

    }

    fun insertUser(user: String, groupId: String, password: String) {
        initRequest()
        this.insert = true

        this.jsonRequest.put(DBContract.DBEntry.USER,  user)
        this.jsonRequest.put(DBContract.DBEntry.GROUP_ID,  groupId)
        this.jsonRequest.put(DBContract.DBEntry.PASSWORD,  password)

        this.downloadAsyncTask!!.execute()
    }

    /**
     * Método que trae obtiene la respuesta de la nube en formato JSON
     */
    fun downloadJSON(): JSONObject {
        val bufferedReader = getBuffer()
        val jsonString = readAll(bufferedReader)

        bufferedReader.close()

        return JSONObject(jsonString)
    }

    /**
     * Método que obtiene el buffer de respuesta de la nube
     */
    private fun getBuffer(): BufferedReader {
        val server: String

        if(this.insert) {
            server = "$URL_SERVER?insert="
            this.insert = false
        }
        else
            server = "$URL_SERVER?peticion="

        val url = URL("$server$jsonRequest")

        return BufferedReader(InputStreamReader(url.openStream()))
    }

    /**
     * Método que lee el contenido completo de BufferedReader
     * @param rd Reader que leeremos
     * @return Cadena con el contenido del Reader
     */
    private fun readAll(rd: Reader): String {
        val stringBuilder = StringBuilder()
        var cp: Int = rd.read()

        while (cp != -1){
            stringBuilder.append(cp.toChar())
            cp = rd.read()
        }

        return stringBuilder.toString()
    }

    override fun processFinish(response: Int) {

    }

    override fun processFinish(json: JSONObject) {

    }

    override fun processFailed() {

    }
}