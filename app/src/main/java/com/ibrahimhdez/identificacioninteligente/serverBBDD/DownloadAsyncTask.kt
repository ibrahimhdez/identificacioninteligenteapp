package com.ibrahimhdez.identificacioninteligente.serverBBDD

import android.os.AsyncTask
import com.ibrahimhdez.identificacioninteligente.faceAPI.asyncTasks.AsyncResponse
import com.ibrahimhdez.identificacioninteligente.utils.Comunicador

class DownloadAsyncTask: AsyncTask<String, Void, Boolean>() {
    var delegate: AsyncResponse? = null

    /**
     * Función que se ejecuta en segundo plano para realizar las descargas de los JSON en la nube
     * @param llamada recibe la llamada a ser ejecutada
     * @return devuelve la llamada que se le pasa por parámetro para iniciar la Activity correspondiente en OnPostExecute
     */
    override fun doInBackground(vararg peticion: String): Boolean {
        try {
            Comunicador.serverManager!!.jsonResult = Comunicador.serverManager!!.downloadJSON()
        } catch (e: Exception) {
            return false
        }
        return true
    }

    /**
     * Método que se ejecuta cuando termina doInBackground
     * @param llamada llamada actual
     */
    override fun onPostExecute(result: Boolean) {
        //Si tenemos un delegado pasamos a ejecutar el método que le corresponda según el resultado de la descarga
        if(this.delegate != null) {
            //Si el doInBackground devuelve una llamada, la descarga se ha hecho correctamente y ejecutamos la función finishProcess de ese delegado
            if (result)
                this.delegate!!.processFinish(Comunicador.serverManager!!.jsonResult)
            //Si devuelve nulo, quiere decir que ha fallado, por lo que ejecutamos el método failedProcess del delegado
            else
                this.delegate!!.processFailed()
        }
    }
}


